\documentclass[../Main.tex]{subfiles}
\begin{document}
\section{Nature du fond}

\subsection{Description du fond (cf.~S-4 -- B-425 à B-427)}
\label{SBDARE}
\index{SBDARE}
Si une zone maritime où la nature du fond est homogène doit être codée, l'objet \textbf{SBDARE} doit être utilisé.

\begin{figure}[H]
	\centering
	\includegraphics{sections/media/image6}
	\caption[Zones de nature de fond]{Zones de nature de fond}
	\label{fig:7.1}
\end{figure}

\begin{objet}
Objet géographique~:& Seabed area (\textbf{SBDARE}) ~ (P, L, A)\\
Attributs~:& COLOUR ~ \underline{NATQUA} ~ \underline{NATSUR} ~ WATLEV ~ OBJNAM ~ NOBJNM\\
&INFORM ~ NINFOM\\
\end{objet}

Remarques~:

Dans les sous-paragraphes suivants, la numérotation se rapporte aux exemples de la Figure \ref{fig:7.1}.

\begin{enumerate}
\def\labelenumi{\alph{enumi})}

\item
Natures du fond multiples~: La nature dominante doit être donnée la première. Lorsque des termes qualificatifs (NATQUA) sont associés aux différentes natures du fond, les termes qualificatifs doivent être listés dans le même ordre que les natures du fond correspondantes.
Lorsqu'une nature particulière n'a pas de terme qualificatif, sa place dans la liste doit être laissée vide et une séparatrice doit être codée. Par exemple, pour coder une nature multiple telle que "sable fin, vase et coquilles brisées", les attributs doivent être codés de la manière suivante : NATSUR \textit{= 4,1,17} et NATQUA=\textit{1,,4}.
Lorsque la dernière nature du fond n'a pas de terme qualificatif, une virgule finale doit être codée. Par exemple, "sable fin et vase" doit être codé par NATSUR = \textit{4},\textit{1} et NATQUA = \textit{1}.
\item Matériaux sous-jacents~: Ils devraient [doivent] être codés de la même façon que les natures multiples, en remplaçant la virgule par une barre oblique (/). Les natures du fond superficielles sont données en premier, suivies par celles des couches sous-jacentes.
\item Récif corallien toujours submergé, représenté par une zone (cf.~INT1 - K16)~: Un objet \textbf{OBSTRN} de type surfacique doit être créé avec CATOBS~=~\textit{6}, NATSUR~=~\textit{14 et WATLEV = 3 (toujours submergé)}. Cet objet doit être couvert par un objet approprié \textbf{DEPARE} ou \textbf{UNSARE}. Dans cette zone, des dangers ponctuels peuvent être représentés. Un objet \textbf{UWTROC} de type ponctuel devrait \shom{[doit]} être créé pour chacun des dangers ponctuels.
\item Fond dur~: L'attribut NATQUA~=~\textit{10} (dur) devrait \shom{[doit]} être codé sans être associé à l'attribut NATSUR.
\item Sur le document source, dans l'estran ou le long de la laisse de basse mer, la nature du fond est parfois figurée par une ligne ouverte plutôt que par une surface fermée. Dans ce cas, un objet \textbf{SBDARE} de type linéaire devrait\footnoteshom{Dans la mesure du possible, les limites ouvertes de zones de nature de fond dans l'estran, doivent être complétées pour délimiter des surfaces de nature homogène. S'il n'est pas possible de compléter la limite sans une interprétation exagérée, un \textbf{SBDARE} de type linéaire doit alors être créé.} être codé, avec WATLEV = \textit{4} (couvre et découvre).
\item Une roche, dangereuse pour la navigation, doit être codée par un \textbf{UWTROC}, tandis qu'une nature de fond rocheuse doit être codée par un \textbf{SBDARE} de type ponctuel.
\item Un \textbf{SBDARE} de type surfacique situé dans l'estran devrait \shom{[doit]} être codé avec WATLEV = \textit{4} (couvre et découvre).

\item Une zone de nature de fond homogène (par exemple une zone rocheuse couvrante et découvrante) doit être codée par un \textbf{SBDARE} de type surfacique avec l'attribut WATLEV renseigné. Dans l'estran et à l'intérieur d'une telle zone, les natures de fond figurées ponctuellement par des abréviations de même nature ne doivent pas être codées.
\item En règle générale, une nature de fond est codée par un \textbf{SBDARE} de \underline{type ponctuel} (on ne cherchera pas à étendre sur une surface l'information donnée par l'abréviation). Cette règle peut cependant ne pas s'appliquer lorsque l'abréviation (R ou Co) est associée à une sonde pour figurer une roche (cf.~§ 6.1.2 et § 6.3.1) ou lorsque l'abréviation est figurée dans l'estran (voir h).
\item La limite supérieure d'un double plateau rocheux parfois représentée dans l'estran sur la carte papier équivalente ne doit pas être codée. La limite inférieure du plateau rocheux suffit pour définir la géométrie du \textbf{SBDARE} décrivant l'ensemble de la zone rocheuse découvrante. Cependant la géométrie de la limite supérieure peut être utilisée pour coder un objet \textbf{SEAARE} afin de traduire un toponyme associé à la partie proéminente de la zone rocheuse.
\item Une zone rocheuse figurée sur la carte papier équivalente et située dans l'estran doit être codée en \textbf{UWTROC} de type ponctuel lorsque sa surface à l'échelle de compilation est inférieure à celle couverte par le symbole K11 (astérisque).
\item La limite d'un récif corallien toujours submergé, parfois représentée sur la carte papier équivalente par un symbole linéaire bleu, peut être fermée ou ouverte. En règle générale, cette information doit être codée par un SBDARE de type linéaire.
\item \textbf{Plateau corallien parsemé de nombreux trous d'eau peu profonds}~:

Les zones qualifiées de «~Plateau corallien parsemé de nombreux trous d'eau peu profonds.~» seront codées sur l'objet \textbf{SBDARE} par les attributs NINFOM = \textit{Plateau corallien parsemé de nombreux trous d'eau peu profonds,} INFORM = \textit{Coral reef with numerous shallow water holes,} et NATSUR = \textit{14} (corail)\textit{.}
\end{enumerate}


\subsection{Natures de fond particulières}

\subsubsection{Fonds mobiles, ridens (cf.~S-4 -- B-428.1)}
\label{SNDWAV}
\index{SNDWAV}
Si des fonds mobiles (ridens) doivent être codés, l'objet \textbf{SNDWAV} doit être utilisé.

\begin{objet}
Objet géographique~:& Sandwaves (\textbf{SNDWAV}) ~ (P, L, A)\\
Attributs~:& \sout{VERACC}\\
&VERLEN - amplitude au-dessus du fond\\
&INFORM ~ NINFOM\\
\end{objet}

\subsubsection{Herbes et algues (cf.~S-4 -- B-428.2)}
\label{WEDKLP}
\index{WEDKLP}
Si des herbes marines ou des algues doivent être codées, l'objet \textbf{WEDKLP} doit être utilisé.

\begin{objet}
Objet géographique~:& Weed / Kelp (\textbf{WEDKLP}) ~ (P, A)\\
Attributs~:& CATWED ~ NOBJNM ~ OBJNAM ~ INFORM ~ NINFOM\\
\end{objet}

\begin{shom_p}
Sur la carte papier équivalente, l'abréviation \textit{Wd} est parfois incluse dans une abréviation composée correspondant à une nature de fond multiple. Deux objets doivent alors être créés, un \textbf{WEDKLP} avec CATWED = \textit{2} pour traduire la partie \textit{Wd} et un \textbf{SBDARE} pour traduire le reste de l'abréviation composée. Ces deux objets doivent être décalés dans l'usage HPD pour éviter une superposition sur l'ECDIS.
On positionnera l'objet \textbf{SBDARE} à sa position réelle et l'objet \textbf{WEDKLP} environ 3 mm au-dessous à l'échelle de compilation.

Lorsque le symbole J13.2 est figuré sur la carte papier équivalente, la valeur d'attribut CATWED = \textit{1} doit être utilisée.
\end{shom_p}

\subsubsection{Sources sous-marines (cf.~S-4 -- B-428.3)}
\label{SPRING}
\index{SPRING}
Si une source sous-marine doit être codée, l'objet \textbf{SPRING} doit être utilisé.

\begin{objet}
Objet géographique~:& Spring (\textbf{SPRING}) ~ (P)\\
Attributs~:& NOBJNM ~ OBJNAM ~ INFORM ~ NINFOM\\
\end{objet}

\subsubsection{Lits de courants de marée (cf.~S-4 -- B-413.3)}
\label{TIDEWY}
\index{TIDEWY}
Si un cours d'eau naturel situé dans une zone d'estran et dans lequel l'eau s'écoule pendant les périodes de flot et de jusant, ou un chenal parcouru par un courant de marée doit être codé\footnoteshom{Un tel objet ne doit être codé que s'il marque l'axe d'un cours d'eau navigable ou celui d'un chenal d'accès à un équipement portuaire ou à un abri naturel.}, l'objet \textbf{TIDEWY} doit être utilisé.

\begin{objet}
Objet géographique~:& Tideway (\textbf{TIDEWY}) ~ (L, A)\\

Attributs~:& NOBJNM ~ OBJNAM ~ INFORM ~ NINFOM\\
\end{objet}

Remarque~:

Le \textbf{TIDEWY} doit être couvert par des objets du Groupe 1 (\textbf{DEPARE, DRGARE} ou \textbf{UNSARE}).

\end{document}