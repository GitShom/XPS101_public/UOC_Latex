\documentclass[../Main.tex]{subfiles}
\begin{document}


\section{Généralités}

Les instructions suivantes précisent les règles qui doivent être appliquées pour coder la description géométrique et sémantique de chaque objet d'une ENC. Dans la mesure du possible, ce document suit l'ordre des paragraphes de la publication ``Règlement de l'OHI pour les cartes internationales (INT) et Spécifications de l'OHI pour les cartes marines -- S-4''.

Ce document décrit comment coder l'information que le cartographe juge utile pour une ENC. L'Autorité Productrice d'une ENC est maître de son contenu, à condition que les règles décrites dans ce document soient appliquées. Une ``Autorité Productrice'' est un Service Hydrographique (SH) ou une autre organisation gouvernementale compétente pour produire des ENC.

Ce document doit être utilisé conjointement avec \shom{la version française  de}  l'édition 2.0 de la Spécification de Produit des ENC (S-57 Appendice  B1, Annexe A) faisant partie de l'édition 3.1 de la S-57 publiée en  novembre 2000, y compris le \shom{Supplément N°3 (juin 2014), et aussi avec  le document "Consignes complémentaires aux documents Spécification de  produit des ENC du SHOM et Utilisation du catalogue d'objets pour les  ENC du SHOM"}.


Bien que ce document soit susceptible de modifications, les	principes suivants doivent être respectés~:

\begin{itemize}
\item \textbf{Aucune modification de ce document ne doit impliquer une mise à jour rétrospective des ENC déjà publiées. Toutefois, les producteurs sont encouragés à intégrer les nouvelles modifications (si elles concernent les données) à toutes nouvelles ENC ou toute ENC pour  laquelle une Nouvelle Édition est programmée. Toute modification nécessaire des données suite à un problème significatif affectant la sécurité de la navigation fera l'objet d'une communication de la part du BHI vers tous les producteurs.}
\item \textbf{Aucune modification de ce document ne doit engendrer un problème à l'utilisation des données dans l'ECDIS.}
\end{itemize}

Des principes et guides complémentaires relatifs à la 	planification, la conception, la production, la mise à jour, la 	distribution et l'affichage des ENC, aux portefeuilles d'ENC et aux ECDIS sont disponibles dans les documents suivants~:

\begin{itemize}
\item Publication M-3~de l'OHI -- Résolutions de l'OHI~; Résolution 1/1997 (telle qu'amendée), Principes de la Base de Données Mondiale pour les Cartes Électroniques de Navigation (WEND)~;
\item Publication S-4~de l'OHI -- Règlement de l'OHI pour les cartes internationales (INT) et Spécifications de l'OHI pour les cartes marines. Noter que les références relatives à la publication S-4 sont indiquées en entête de chapitres dans le présent document~;
\item Publication S-52~de l'OHI -- Spécifications pour le contenu cartographique et les modalités d'affichage des ECDIS~;
\item Publication S-62~de l'OHI -- Codes des producteurs d'ENC~;
\item Publication S-63~de l'OHI -- Dispositif de l'OHI pour la protection des données~;
\item Publication S-65~de l'OHI -- Guide de production des ENC~;
\item Publication S-66~de l'OHI -- La carte marine et les	prescriptions d'emports~: les faits~;
\item Résolution A.817 (19) de l'OMI, telle qu'amendée par MSC.64 (67) et MSC.86 (70) -- Normes de fonctionnement des Systèmes de visualisation de cartes électroniques et d'information (ECDIS).
\end{itemize}

En outre, le document suivant est issu de la norme S-57 de l'OHI et intègre en grande partie le présent document~:

\begin{itemize}
\item \textbf{Publication S-58~de l'OHI -- Vérifications recommandées par l'OHI pour la validation des ENC.}
\end{itemize}

\textbf{Note}~: le sous-chapitre 3.5.7 de la Spécification de Produit pour les ENC fournit un guide pour le codage de l'attribut INFORM afin de décrire la signification des valeurs d'attribut nouvellement apparues lors de l'Édition 3.1 de la S-57, pour des raisons de compatibilité	rétroactive avec l'Édition 3.0 de la S-57. De même, les sous-chapitres 3.3.1, 3.5.2.1 et 3.5.7.1 de la section 4 du supplément \shom{N°3 (juin 2014)} à l'Édition 3.1 de la S-57 fournit un guide pour le codage de l'attribut INFORM afin de décrire la signification des objets et des valeurs d'attribut nouvellement apparus dans le supplément N°1 (janvier 2007) de l'Édition 3.1 de la S-57.

L'Édition 3.4 (Janvier 2008) de la Bibliothèque de Présentation de l'OHI (S-52 Appendice 2, Édition 4.3 -- Spécifications pour les Couleurs et Symboles pour les ECDIS, Annexe A) n'exige plus de coder INFORM lorsque ces objets ou ces valeurs d'attribut sont renseignés.

Les encodeurs sont informés qu'il n'est plus nécessaire de	renseigner INFORM pour décrire la signification des objets et des valeurs d'attribut nouvellement apparus dans l'Édition 3.1 de la S-57 ou dans le Supplément N°1.

Pour les ENC en service, l'affichage sur l'ECDIS ne sera pas affecté par le maintien des valeurs d'attribut renseignées dans INFORM.


\begin{shom_p}
\textbf{Principes généraux : une ENC du SHOM est issu du même fond cartographique (au format S-57) que celui utilisé pour créer la ou les cartes papier équivalente(s)}\footnoteshom{Ceci ne concerne pas les ENC réalisées dans le cadre de l'extension du portefeuille d'ENC du SHOM. Dans ce cas, les informations de l'ENC proviennent exclusivement de la carte papier en service (Carte de référence) et de la BDGS.}.

Pour constituer le fonds cartographique, les sources d'information (à jour) utilisées sont :

\begin{itemize}
\item La Base de Données Générale du SHOM (BDGS) (cf. § 1.5), lorsque la zone est couverte par les données de la BDGS ;
\item A défaut : les cartes papier équivalentes, complétées du Livre des Feux et Signaux de brume pour les objets qui y sont décrits ;
\item Pour les autres thèmes : toutes les informations (sous forme numérique ou papier) dont dispose le cartographe : levés bathymétriques (BDBS), plans, cartes papier (SHOM ou étrangères), photographies aériennes ou imagerie satellite, publications nautiques, etc.).
\end{itemize}

\textbf{Les règles détaillées à appliquer sont précisées dans les chapitres correspondants du présent document, à utiliser en complément de la norme S-4 et du CCPCM.}

\textbf{Préparation du codage d'une ENC :} Les prescriptions de codage complémentaires aux règles du présent document doivent être inscrites dans le cahier de définition donnant les caractéristiques de l'ENC.

Le dossier de définition d'une ENC peut prescrire des dérogations aux règles du présent document.
\end{shom_p}




\subsection{Références dans la S-57 à d'autres publications de l'OHI}

Dans l'ensemble de la documentation S-57, il existe des références à des chapitres d'autres publications de l'OHI, notamment la S-4 -- Règlement de l'OHI pour les cartes internationales (INT) et Spécifications de l'OHI pour les cartes marines, et l'INT1 -- Symboles, abréviations et termes utilisés sur les cartes marines. La norme S-57 a été "gelée" depuis 2000 mais, ces autres publications ayant fait l'objet de révisions, les références mentionnées dans les documents S-57 peuvent être incorrectes. Il est à noter qu'à sa date de publication, les références mentionnées dans le présent document sont correctes pour les versions en vigueur des publications de l'OHI.

Les encodeurs doivent aussi noter que les numéros d'index du Dictionnaire Hydrographique de l'OHI, Édition 5, mentionnés dans les chapitres 1 et 2 (Objets et Attributs) de la S-57 peuvent se référer à des définitions ayant été révisées ou remplacées. Les encodeurs devraient donc utiliser le Dictionnaire Hydrographique en ligne de l'OHI, qui est accessible sur le site web de l'OHI.

\section{Présentation du document}

Les conventions suivantes sont utilisées~:

\begin{itemize}
\item
Conventions de présentation~: \newline
\begin{tabular}{ll}
Classe d'objets~:& \textbf{WRECKS}\\
Primitive géométrique~:& (P, A)*\\
Attribut~:& EXPSOU\\
Attribut obligatoire~:& \underline{WATLEV}\\
Attribut interdit~:& \sout{VERDAT}\\
Valeur d'attribut :& \textit{-2.4}\\
\end{tabular}
\end{itemize}



\begin{itemize}
\item \shom{Particularités des ENC du SHOM : Dans ce document, les textes en caractères bleus (\modif{en magenta pour les consignes ajoutées lors de la présente édition}) du style Times New Roman précisent les particularités des ENC produites par le SHOM.}
\item \ohi{Dans ce document, les textes en caractères verts correspondent à des règles données dans des bulletins de codage de l'OHI ou à des compléments d'information définis au niveau des groupes de travail de l'OHI émis postérieurement à l'édition 4.0.0.}
\end{itemize}

\begin{itemize}
\item Attributs\_A~: Pour chaque classe d'objets, la liste complète des attributs du type A est donnée avec, lorsqu'elles sont nécessaires,	les valeurs particulières propres à l'objet sémantique. Pour toute	information sur les attributs obligatoires sous certaines conditions, se référer à la S-57 Appendice B1 (Spécification de Produit des ENC) - Paragraphe 3.5.2.
\item Attributs\_B, attributs\_C~: Excepté pour les attributs INFORM, NINFOM, et plus rarement SORDAT, les attributs des types B et C ne sont pas mentionnés dans les listes suivantes ; cependant cela ne signifie pas que leur usage soit interdit.
\end{itemize}

Pour la définition des attributs de type A, B et C, consulter l'Appendice A - Chapitre 1 - Paragraphe 1.1.

* Pour les primitives géométriques~: P = point~; L = ligne~; A = surface~; N = aucune.

\section{Éléments de langage}

Dans ce document :

\begin{itemize}
\item Le terme "doit" \shom{("must" dans le document original en anglais)} indique une exigence à respecter obligatoirement.
\item Le terme "devrait" \shom{("should" dans le document original en anglais)} indique une	exigence optionnelle, c'est-à-dire une règle à laquelle il est recommandé de se conformer, mais qui n'est pas obligatoire.
\item Le terme "peut" \shom{("may" dans le document original en anglais) } indique une autorisation ou une possibilité.
\end{itemize}

\shom{Les règles optionnelles mises en évidence dans le document original par les termes "devrait" et "peut" sont précisées pour les ENC du SHOM par des renvois en fin de paragraphe ou par des mentions [doit] ajoutées immédiatement après "devrait" ou "peut".}

\section{Tenue à jour}

\textbf{Les modifications de ce document sont coordonnées par le Groupe de Travail sur la maintenance et le développement d'applications de la norme de transfert (TSMAD). Toute personne souhaitant apporter des modifications au document doit adresser ses commentaires au TSMAD.}

\textbf{Il existe trois types de propositions de modification au Guide d'Utilisation du Catalogue d'Objets pour les ENC :}

\begin{enumerate}
\item Clarification
\item Révision
\item Nouvelle Édition
\end{enumerate}

Toute proposition de modification doit respecter une de ces formes. Il est à noter que, la S-57 ayant été «~gelée~» (à l'exception du présent document), toute modification à ce document doit concerner uniquement les prescriptions de codage pour la compilation d'ENC conformes aux règles de l'OHI, et ne doivent pas modifier ou être contraire aux règles et conventions décrites dans tous les autres documents S-57, y compris aux clarifications du Document cumulatif 8 de mise à jour de la S-57 (S-57 MD8).

\textbf{TOUTES} les modifications proposées doivent être validées techniquement avant approbation. Toute révision ou modification significative devant être introduite par une Nouvelle Édition devra normalement être d'abord publiée sur le site de l'OHI (www.iho.int) en tant que Bulletin de Codage pour ENC et/ou Question Fréquemment Posée (FAQs) sur la page du TSMAD. Si le sujet est considéré comme constituant un problème vis-à-vis de la sécurité de navigation, il sera également diffusé par voie de Lettre Circulaire de l'OHI.

Les modifications approuvées doivent être intégrées au présent document et répertoriées sur la page «~Gestion de Document~».

\subsection{Clarification}

Les clarifications sont des modifications non substantielles du document. Typiquement, une clarification lève une ambiguïté, corrige des erreurs grammaticales ou d'orthographe, modifie ou met à jour des références croisées ou améliore des illustrations. Une clarification ne doit pas apporter de modification de fond substantielle au document.

\subsection{Révision}

Les révisions sont des modifications de fond substantielles du document.Typiquement, une révision modifie le document pour corriger des erreurs avérées ou pour introduire des modifications nécessaires aux prescriptions de codage des ENC, devenues évidentes au vu de l'expérience pratique ou de changement de circonstances. Une révision ne doit pas être aussi enregistrée en tant que clarification. Les révisions peuvent avoir un impact sur les utilisateurs courants ou futurs du document. Toutes les clarifications cumulatives doivent être intégrées aux publications de révisions approuvées.

\subsection{Nouvelle Édition}

Les Nouvelles Éditions sont des modifications significatives des prescriptions de codage du document, ces modifications ne devant pas changer ou être contraire aux règles et conventions décrites dans l'ensemble des autres documents S-57. Une Nouvelle Édition peut intégrer des informations complémentaires provenant du TSMAD ou d'un autre comité, et qui ne figuraient pas précédemment dans le document. Une Nouvelle Édition constitue une importante nouvelle version du document. Une même Nouvelle Édition peut engendrer de multiples actions. Toutes les clarifications et révisions cumulatives doivent être intégrées à la publication de la Nouvelle Édition approuvée. Après approbation, la
Nouvelle Édition est disponible pour mise en œuvre à une date précisée par le TSMAD.

\subsection{Gestion de versions}

Le TSMAD doit publier autant de nouvelles versions du document que nécessaire. Les nouvelles versions doivent intégrer les clarifications, les corrections et les compléments. Chaque version doit inclure une liste identifiant les modifications entre deux versions successives. 

\subsubsection{Compteur de la version de clarification}

Les Clarifications doivent être identifiées sous la forme : 0.0.x. Toute clarification ou ensemble de clarifications approuvé à une certaine date doit incrémenter x de 1.

\subsubsection{Compteur de la version de révision}

Les Révisions doivent être identifiées sous la forme 0.x.0. Toute révision ou ensemble de révisions approuvé à une certaine date doit incrémenter x de 1. Toute nouvelle version de révision entraîne une remise à zéro du compteur de version de clarification.

\subsubsection{Compteur de la version de Nouvelle Édition}

Les Nouvelles Éditions doivent être identifiées sous la forme x.0.0. Toute Nouvelle Édition approuvée à une certaine date doit incrémenter x de 1. Toute Nouvelle Édition entraîne une remise à zéro du compteur de version de clarification et de révision.

\section{Règles générales d'exploitation de la BDGS}
\begin{shom_p}
La BDGS comporte certains types d'objets, dont notamment les dangers ponctuels artificiels (épaves, fonds malsains et obstructions), les amers, les marques de balisage et les alignements, les zones réglementées et les câbles (se référer aux spécifications de la BDGS).

La description des objets dans la BDGS (la source) est au format S-57, mais ne se conforme pas à la spécification de produit pour ENC (le produit). Le catalogue d'objets et d'attributs de la BDGS diffère de celui du produit ENC.

Ainsi, certains objets de la BDGS peuvent avoir un attribut (ou une valeur d'attribut) qui est interdit(e) dans une ENC. Dans ce cas, l'attribut sera automatiquement bloqué ou modifié par l'outil de production pour que le produit ENC ou l'ER soit conforme à la spécification de produit. Ce mécanisme est appelé « mapping » dans le présent document et est donc appliqué entre la source et le produit ENC.

La traduction d'un objet de la BDGS est unique quelle que soit l'échelle de compilation, sauf lorsqu'il y a changement de primitive géométrique, auquel cas la filiation avec la BDGS peut ne plus être assurée. ~~Des exceptions à cette règle existent pour la traduction des marques de balisage (cf. §12.3 et §12.8.1).~~ La valeur de l'attribut spatial QUAPOS peut varier en fonction de POSACC et de l'échelle de l'usage HPD.

Les objets sémantiques traduits à partir d'un même objet de la BDGS et figurant sur différentes ENC doivent avoir le même identifiant (FOID).

De la même manière, deux objets distincts traduits dans la même cellule à partir d'un seul objet de la BDGS et dont les géométries sont disjointes (par exemple, un câble de part et d'autre d'une zone sans données), doivent tous deux être codés avec le même FOID que celui de l'objet BDGS. La gestion de la représentation de ces objets doit donc se faire par masquage des edges dans l'usage HPD ou dans le produit ENC (lorsque l'on souhaite 2 représentations différentes pour 2 ENC issues du même usage HPD).

Le tableau ci-dessous identifie les objets du fonds cartographique sur lesquels la filiation avec la BDGS est (ou n'est pas) assurée.
\end{shom_p}

\include{sections/tables/Filiation_BDGS}
\end{document}