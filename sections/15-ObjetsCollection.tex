\documentclass[../Main.tex]{subfiles}
\begin{document}

\label{C_AGGR}
\index{C\_AGGR}
Si une aggrégation ou une association entre plusieurs objets doit être identifiée, un objet \textbf{C\_AGGR} ou \textbf{C\_ASSO}\footnoteshom{Sauf dérogation mentionnée dans le cahier de définition, l'objet
\textbf{C\_ASSO} ne doit pas être utilisé.} doit être utilisé.

\begin{objet}
Objet collection~:&Aggrégation (\textbf{C\_AGGR}) ~ (N)\\

Attributs~:& NOBJNM ~ OBJNAM ~ INFORM ~ NINFOM\\
\end{objet}

Le \textbf{C\_AGGR} devrait \shom{[doit]} être utilisé pour coder le lien fonctionnel entre des objets formant ensemble une entité de niveau supérieur. Par exemple, une ligne de position, une voie recommandée et les aides à la navigation concernées devraient être liées par un \textbf{C\_AGGR} pour former un système d'alignement.
\label{C_ASSO}
\index{C\_AGGR}
\begin{objet}
Objet collection~:&Association (\textbf{C\_ASSO}) ~ (N)\\

Attributs~:& NOBJNM ~ OBJNAM ~ INFORM ~ NINFOM\\
\end{objet}

Le \textbf{C\_ASSO} devrait\textsuperscript{\shom{(1)}} être utilisé pour coder l'association entre deux ou plusieurs objets. L'association ne crée pas d'entité de niveau supérieur. Par exemple, une épave devrait être associée à la bouée qui la balise.

Le tableau suivant récapitule les exemples les plus courants d'utilisation d'objets collection. D'autres utilisations d'objets collection sont possibles.

\begin{longtable}[c]{|L{0.4\linewidth}|c|l|l|}
	\hline
	\rowcolor[HTML]{C0C0C0} 
	\textbf{Relations} & \multicolumn{1}{l|}{\cellcolor[HTML]{C0C0C0}\textbf{Champ FFPT}} & \textbf{Objet collection} & \textbf{Commentaire} \\ \hline
	\endhead
	%
	Embossages & pair * & C\_AGGR & voir   § 9.2.5 \\ \hline
	Distances   mesurées & pair * & C\_AGGR & voir   § 10.1.3 \\ \hline
	Systèmes   d'organisation du trafic & pair * & C\_AGGR & voir   § 10.2.3 \\ \hline
	Systèmes   d'alignement & pair * & C\_AGGR & voir   § 10.1.2 \\ \hline
	Systèmes   d'alignement et dangers & pair * & C\_ASSO & voir   § 10.1.2 ** \\ \hline
	Feux   synchronisés & pair * & C\_ASSO & voir   § 12.8.7 \\ \hline
	Aéroports,   aérodromes (pistes, contrôle, etc.) & pair * & C\_ASSO & voir   § 4.8.12 \\ \hline
	Marée,   courants de marée (prédiction non harmonique, séries chronologique ou   prédiction harmonique) & pair * & C\_ASSO & voir   § 3.2.3, 3.3.3 et 3.3.4 \\ \hline
	Dispositifs   de mouillage & pair * & C\_ASSO & voir   § 9.2.6 \\ \hline
	Chenaux & pair * & C\_ASSO/C\_AGGR & voir   § 10.4 \\ \hline
	&  &  &  \\ \hline
	Balises   radar & pair * & C\_AGGR & voir   § 12.10 \\ \hline
	\caption{utilisation d'objets collection}
	\label{tab:15.1}\\
\end{longtable}
%Tableau 15.1

* Les relations utilisant des objets collection sont considérées de pair à pair. Le sous-champ RIND des enregistrements de ces objets collection doit être "pair" (voir spécification de produit des ENC, § 3.9).

** Un tel \textbf{C\_ASSO} ne doit pas être utilisé pour lier les éléments individuels composant l'alignement, mais il devrait être utilisé pour montrer la relation entre le système "alignement, voie recommandée" et les dangers associés.

\textbf{Les relations peuvent être hiérarchiques}.

Si un objet collection s'étend au-delà des limites d'une cellule (c'est-à-dire que les objets qui composent la collection sont répartis sur plusieurs cellules), l'objet collection devrait \shom{[doit]} être répété dans chaque cellule contenant un ou plusieurs des éléments composant la collection. Toutefois, seuls les objets existant dans la cellule contenant un exemplaire de l'objet collection peuvent être référencés par cet objet collection. Si cette technique est utilisée\textsuperscript{(2)}, chaque exemplaire de l'objet collection original doit avoir le même identificateur d'élément (FOID). C'est à l'application (ex : l'ECDIS) qui utilise les cellules de reconstruire l'objet collection complet à partir de cet unique identificateur d'élément.

Il est fortement recommandé de ne pas utiliser de pointeurs qui font référence à des objets extérieurs à la cellule dans laquelle les pointeurs sont codés. L'utilisation de tels pointeurs ne peut être interdite car il n'existe pas de règle l'interdisant dans la spécification de produit ENC.

Remarque~:

\begin{itemize}
\tightlist
\item Les directives sur l'utilisation (affichage et interrogation) des objets \textbf{C\_AGGR} et \textbf{C\_ASSO\textsuperscript{\shom{(1)}}} dans l'ECDIS n'ont pas été intégrées dans les documents de l'OMI, du CEI et de l'OHI relatifs aux performances et aux aspects d'affichage de l'ECDIS. De ce fait, la plupart des ECDIS n'ont pas la capacité d'afficher ou d'interroger ces objets lorsqu'ils sont codés. En conséquence, le encodeurs ne doivent pas coder d'informations significatives relatives à la navigation en utilisant les attributs autorisés de \textbf{C\_AGGR} et \textbf{C\_ASSO} (ex~: OBJNAM, NOBJNM, TXTDSC et NTXTDS). Si le nom d'un objet composé (tel qu'un dispositif de séparation du trafic) doit être indiqué, il devrait \shom{[doit]} être codé par un objet \textbf{SEAARE}, \textbf{LDNRGN} ou \textbf{ADMARE} (cf.~§ 14), ou en renseignant OBJNAM du plus représentatif des objets de la collection. Si une information textuelle sur un objet composé doit être codée, l'objet \textbf{M\_NPUB} (cf.~§ 15) devrait \shom{[doit]} être utilisé, avec les attributs INFORM / NINFOM et/ou TXTDSC / NTXTDS (cf.~§ 2.3), ou l'objet \textbf{CTNARE} (cf.~§ 6.6) doit être utilisé, si l'information est jugée essentielle pour la sécurité de la navigation.
\end{itemize}

\end{document}