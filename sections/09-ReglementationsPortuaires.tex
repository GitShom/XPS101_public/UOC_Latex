\documentclass[../Main.tex]{subfiles}
\begin{document}

\section{Réglementations à l'intérieur des limites de ports}

\subsection{Zones portuaires administratives (cf.~S-4 -- B-430.1)}
\label{HRBARE}
\index{HRBARE}
Si une zone portuaire administrative doit être codée, l'objet 	\textbf{HRBARE} doit être utilisé.

\begin{objet}
Objet géographique~:& Harbour area (\textbf{HRBARE}) ~ (A)\\

Attributs~:& NOBJNM ~ OBJNAM ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

Remarque~:

Une ligne masquée peut être utilisée pour supprimer la représentation symbolique de la limite lorsque cette symbolisation est considérée inappropriée.

\shom{Seules les limites en mer des zones administratives portuaires sont généralement représentées sur les cartes papier équivalente ; aussi, les primitives géométriques des objets \textbf{HRBARE} de type surfacique doivent être limitées vers le large par les limites de circonscription figurées sur les cartes papier équivalente et vers la terre par le trait
de côte, à l'intérieur des ports elles couvrent les parties maritimes limitées par les premiers ouvrages portuaires (quais, ponts, portes d'écluses, etc.).}

\subsection{Limites de vitesse (cf.~S-4 -- B-430.2)}

La vitesse est souvent limitée dans les ports pour éviter que les sillages n'engendrent des vagues. Si une telle zone de restriction doit être codée, l'objet \textbf{RESARE} doit être utilisé, avec CATREA~=~\textit{24} (zone de sillage interdit) ou RESTRN = \textit{13} (sillage interdit). Si des cas où la vitesse limite est connue doivent être codés, l'attribut RESTRN = \textit{27} (vitesse limitée) doit être utilisé et la vitesse limite ainsi que son unité de mesure doivent être codées avec INFORM (ex : INFORM = \textit{Speed limit is 5 knots}) \shom{et NINFOM (ex : \textit{La vitesse est limitée à 5 nœuds})}.

Si les bouées et/ou les balises marquant la \textbf{RESARE} où la vitesse est limitée doivent être codées, l'objet \textbf{BCNSPP} ou \textbf{BOYSPP} doit être utilisé (cf.~§ 12.3.1 et 12.4.1) avec l'attribut CATSPM~=~\textit{24} (marque de sillage réduit) ou \textit{25} (marque de limitation de vitesse). La vitesse limite et son unité de mesure devraient \shom{[doivent]} être codées par INFORM (ex : INFORM = \textit{Speed limit is 5 knots}) \shom{et NINFOM (ex : \textit{La vitesse est limitée à 5 nœuds})}.

\section{Mouillages et mouillages réglementés ou interdits}

\subsection{Mouillages (cf.~S-4 -- B-431.1~; et 431.3 et B-431.7)}

Si une zone de mouillage doit être codée, l'objet \textbf{ACHARE} doit être utilisé.

\begin{objet}
Objet géographique~:& Anchorage area (\textbf{ACHARE}) ~ (P, A)\\
Attributs~:& CATACH ~ DATEND ~ DATSTA ~ NOBJNM\\
&OBJNAM - nom ou numéro du mouillage\\
&PEREND ~ PERSTA ~ RESTRN ~ STATUS\\
&INFORM - informations supplémentaires sur la catégorie du mouillage\\
&NINFOM\\
\end{objet}

Remarques~:

Un mouillage  signalé, isolé et sans limite définie (N10) devrait \shom{[doit]} être codé par un \textbf{ACHARE} de type ponctuel avec CATACH = \textit{1} (mouillage sans restriction), STATUS~=~\textit{3} (recommandé), INFORM = \textit{Reported anchorage} et NINFOM = \textit{Mouillage signalé}.

Le toponyme (codé par l'attribut OBJNAM) d'un objet \textbf{ACHARE} de type surfacique ne s'affiche pas sur l'ECDIS. Lorsque l'on considère qu'il est nécessaire d'afficher le nom d'une zone de mouillage sur l'ECDIS, un objet \textbf{SEAARE} de type surfacique, de même géométrie que l'objet \textbf{ACHARE}, devrait \shom{[doit]} être codé. L'attribut OBJNAM du \textbf{SEAARE} devrait \shom{[doit]} être codé avec le nom du mouillage tel que codé sur l'objet \textbf{ACHARE}.

Une zone avec de nombreux postes d'amarrage pour embarcations (cf.~S-4 - B-431.7) peut\footnoteshom{La zone de mouillage pour embarcations doit être codée conformément à sa représentation graphique (surfacique ou ponctuelle) sur la carte  papier équivalente.} être codée par un \textbf{ACHARE} de type surfacique avec CATACH~=~\textit{8} (zone d'amarrage pour embarcations). Pour le codage des bouées d'amarrage, voir § 9.2.4.

Si un mouillage pour hydravions doit être codé, CATACH = \textit{6} doit être utilisé (mouillage pour hydravions).

\begin{itemize}
\item Si un mouillage ne pouvant être utilisé que pendant une période inférieure à 24 heures doit être codé, CATACH = \textit{9} (mouillage pour périodes inférieures ou égales à 24 heures) doit être utilisé. \item Si un mouillage ne pouvant être utilisé que pendant une période spécifique limitée doit être codé, CATACH = \textit{10} (mouillage pour une durée limitée) doit être utilisé. La limite de la durée devrait être codée par l'attribut INFORM (ex : \textit{Anchorage limited to 12 hours}) \shom{et NINFOM (ex : \textit{Mouillage limité à 12 heures).}} 
\item \textit{Les zones dans lesquelles le mouillage est interdit doivent être codées, si besoin, par des \textbf{RESARE} (voir § 11.1) avec RESTRN = 1 (mouillage interdit).} 
\item \shom{Si une zone de mouillage d'attente (à distinguer d'une zone d'attente) doit être codée, l'objet \textbf{ACHARE} doit être utilisé avec INFORM = \textit{Waiting anchorage area} et NINFOM = \textit{Zone de mouillage d'attente}.}
\end{itemize}

\shom{Exemples de codage de mouillages :}

\begin{table}[H]
\centering
\begin{shom_p}
\begin{tabular}{|L{0.25\linewidth}|c|C{0.1\linewidth}|c|L{0.25\linewidth}|}
\hline
\rowcolor[HTML]{C0C0C0} 
\textbf{Représentation sur la carte papier équivalente}&\textbf{INT 1}&\textbf{CATACH}&\textbf{STATUS}&\textbf{Commentaire}\\\hline
Ancre sans légende, imprimée en noir & N10 & 1  \textit{(mouillage sans restriction)} & 3 \textit{(recommandé)} & INFORM = Reported   anchorage NINFOM = Mouillage signalé \\ \hline
Ancre   sans légende, imprimée en magenta & N12.1 & 1 & 3 \textit{(recommandé)} & Le symbole peut   être figuré dans une zone. \\ \hline
Ancre   avec la légende "Plaisance", imprimées en magenta & & 7   \textit{(mouillage pour embarcations)} & \textit{(réservé)} & Le symbole et la légende   associée peuvent être figurés dans une zone. INFORM = Boating  NINFOM = Plaisance  La valeur CATACH = 8 doit être utilisée   lorsque la mention "sur coffres" ou "sur bouées" est   indiquée. \\ \hline
Légende   noire "Mouillage pour embarcations" à l'intérieur d'une limite en   trait tireté noir & Q44 & 7 & & La valeur CATACH = 8 doit être utilisée lorsque la   mention "sur coffres" ou "sur bouées" est indiquée. \\ \hline
Légende "Mouillage sur bouées" imprimée en magenta   (sans limite représentée) & & 8 \textit{(zone d'amarrage pour   embarcations)} & & Un ou plusieurs objets ACHARE de type ponctuel   répartis sur la légende doivent être créés. L'objet spatial associé doit être   codé avec QUAPOS = 4. \\ \hline
\end{tabular}
\end{shom_p}
\caption{Mouillages}
\label{tab:9.1}
\end{table}


\subsection{Postes de mouillage (cf.~S-4 -- B-431.2)}

Si un poste de mouillage doit être codé, l'objet \textbf{ACHBRT} doit être utilisé.

\begin{objet}
Objet géographique~:& Anchorage berth (\textbf{ACHBRT}) ~ (P, A)\\
Attributs~:& CATACH ~ DATEND ~ DATSTA ~ NOBJNM\\
&OBJNAM - nom ou numéro du poste de mouillage\\
&PEREND ~ PERSTA\\
&RADIUS - rayon du cercle d'évitage en mètres\\
&STATUS\\
&INFORM - informations supplémentaires sur la catégorie du mouillage\\
&NINFOM\\
\end{objet}

Remarque~:

Si le poste de mouillage est défini par le point central et le rayon du cercle d'évitage, l'objet spatial associé devrait\footnoteshom{Le poste de mouillage doit être codé conformément à sa représentation graphique (surfacique ou ponctuelle) sur la carte  papier équivalente.} être de type ponctuel, le rayon du cercle d'évitage étant codé par l'attribut RADIUS.

\subsection{Mouillages réglementés (cf.~S-4 -- B-431.4)}

Si une zone de mouillage interdit ou réglementé doit être codée, l'objet \textbf{RESARE} (cf.~§ 11.1) doit être utilisé, avec RESTRN~=~\textit{1} (mouillage interdit), \textit{2} (mouillage réglementé) ou \textit{7} (entrée interdite). Les précisions sur les restrictions devraient\footnoteshom{Ces précisions sont codées si elles sont figurées sur la carte  papier équivalente (dans la zone elle-même ou dans un nota).} être codées dans INFORM \shom{(et NINFOM)} ou TXTDSC\footnoteshom{TXTDSC ne doit pas être utilisé.}.

\subsection{Coffres ou bouées d'amarrage (cf.~S-4 -- B-431.5)}

Si une bouée d'amarrage doit être codée, l'objet \textbf{MORFAC} (cf.~§ 4.6.7.1) doit être utilisé, avec CATMOR~=~\textit{7} (bouée d'amarrage).

\shom{La couleur d'un coffre d'amarrage ne doit être codée que lorsque celle-ci est indiquée sur la carte papier équivalente.}

\subsection{Embossages (cf.~S-4 -- B-431.6)}

\begin{figure}[H]
	\centering
	\includegraphics{sections/media/image7}
	\caption[Embossage]{Embossage}
	\label{fig:9.1}
\end{figure}


Un embossage complet est composé de systèmes d'ancrage, de câbles d'amarrage, de coffres et de postes d'amarrage sur des câbles de jonction. Les numéros des sous-paragraphes suivants se rapportent aux exemples de la Figure 9.


\begin{enumerate}
\def\labelenumi{\alph{enumi})}
\item Les systèmes d'ancrage devraient \shom{[doivent]} être codés par des \textbf{OBSTRN} (cf.~§ 6.2.2) avec CATOBS~=~\textit{9} (système d'ancrage).
\item Les câbles d'amarrage devraient \shom{[doivent]} être codés par des \textbf{CBLSUB} (cf.~§ 11.5.1) avec CATCBL~=~\textit{6} (câble ou chaîne d'amarrage).
\item Les coffres devraient \shom{[doivent]} être codés par des \textbf{MORFAC} (cf.~§ 4.6.7.1) avec CATMOR~=~\textit{7} (coffre ou bouée d'amarrage).
\item Les postes de mouillage entre coffres devraient \shom{[doivent]} être codés par des \textbf{BERTHS} (cf.~§ 4.6.2).
\item Les câbles de jonction devraient \shom{[doivent]} être codés par des \textbf{MORFAC} (cf.~§ 4.6.7.1) avec CATMOR~=~\textit{6} (chaîne/cable).
Tous ces objets devraient \shom{[doivent]} être associés en utilisant un objet collection \textbf{C\_AGGR} (cf.~§ 15).

\end{enumerate}
\subsection{Dispositifs de mouillage - Relations}

Pour coder un dispositif de mouillage, des objets tels que des \textbf{ACHARE}, \textbf{ACHBRT}, \textbf{MORFAC}, \textbf{RESARE}, \textbf{C\_AGGR} (embossage) et des objets décrivant les aides à la navigation peuvent\footnoteshom{Sauf dérogation mentionnée dans le cahier de définition, l'objet \textbf{C\_ASSO} ne doit pas être utilisé.} être associés par un objet collection \textbf{C\_ASSO} (cf.~§ 15).

\end{document}