\documentclass[../Main.tex]{subfiles}
\begin{document}
\section{Objets variables dans le temps}

\subsection{Données magnétiques (cf.~S-4 -- B-270)}

\shom{Dans tous les cas, les prescriptions relatives à ce thème doivent être fournies par le SHOM dans le cahier de définition.}

\subsubsection{Déclinaison magnétique}
\label{MAGVAR}
Jusqu'à ce qu'un modèle magnétique mondial soit disponible pour être inclus dans l'ECDIS, si la déclinaison magnétique doit être codée, l'objet \textbf{MAGVAR} doit être utilisé. Au minimum, les diffusions de mises à jour doivent coïncider avec les changements d'époques (tous les cinq ans).


\begin{objet}
	Objet géographique~:& Magnetic variation (\textbf{MAGVAR}) (P, L, A)\\
	Attributs~:& DATEND  ~ DATSTA  ~ \underline{RYRMGV}  ~ \underline{VALACM}  ~ \underline{VALMAG}  ~ INFORM  ~ NINFOM
\end{objet}


Remarques~:

\begin{itemize}
	\item Il est toujours requis de coder l'information relative à la déclinaison magnétique dans les ENC, étant donné que la réglementation SOLAS impose l'emport d'un compas et d'une courbe de déviation. Les retours d'utilisateurs montrent qu'il peut être difficile d'accéder aux informations relatives à la déclinaison magnétique sur l'ECDIS lorsque celles-ci sont codées par des primitives ponctuelles ou linéaires. Afin de rendre l'information relative à la déclinaison magnétique disponible pour les utilisateurs d'ECDIS, il est recommandé de coder cette information à l'aide d'objets \textbf{MAGVAR} de type surfacique. Le codage de cette information par une primitive géométrique surfacique permet à l'utilisateur de pouvoir interroger les données de l'ENC à l'aide de la fonction «~Pick Report~» de l'ECDIS à partir de n'importe quelle position, pour identifier la valeur de la déclinaison magnétique en ce point.
\end{itemize}

\begin{shom_p}
Lorsqu'une partie navigable est figurée dans une ENC, celle-ci doit contenir au moins un \textbf{MAGVAR}.

Les valeurs des déclinaisons magnétiques et des changements annuels adoptées lors du codage sont celles calculées d'après le dernier modèle magnétique disponible. Les valeurs codées sont arrondies selon les mêmes règles que celles de la carte papier (la déclinaison est indiquée à 5' près, la variation à 1' près). Le type de l'information (ponctuel, linéaire ou surfacique) est fonction de la représentation sur la carte papier équivalente (roses ou isogones) et du "type de navigation" (INTU) de l'ENC, selon la règle ci-dessous.

Pour les ENC dont le "type de navigation" est 5 ou 6, la partie maritime (c'est à dire celle au large du trait de côte) doit être couverte par un ou plusieurs \textbf{MAGVAR} de type surfacique. Les attributs d'un tel objet sont renseignés à partir des résultats du calcul du magnétisme effectué à la position approximative du barycentre de la surface associée à l'objet.

Pour les ENC dont le "type de navigation" est 1, 2, 3 ou 4, si les informations sur le magnétisme sont figurées sur la carte papier équivalente par des roses de déclinaison, celles-ci sont codées à la même position par des \textbf{MAGVAR} de type ponctuel. Si les informations sur le magnétisme sont figurées par des isogones, celles-ci sont codées à la fois par des \textbf{MAGVAR} de type linéaire dont l'attribut VALACM est codé "unknown" et par des objets ponctuels de la même classe avec VALACM et VALMAG renseignés pour traduire les points de ces isogones oú le changement annuel est indiqué.
\end{shom_p}

\subsubsection{Déclinaison magnétique anormales (cf.~S-4 -- B-274)}
\index{LOCMAG}
\label{LOCMAG}
Si une déclinaison magnétique anormale doit être codée dans une zone donnée, l'objet \textbf{LOCMAG} doit être utilisé.

\begin{objet}
	Objet géographique~:& Local magnetic anomaly (\textbf{LOCMAG}) (P, L, A)\\
	Attributs~:& NOBJNM  ~ OBJNAM  ~ \underline{VALLMA}  ~ INFORM  ~ NINFOM\\
\end{objet}

Si la zone ne peut pas être définie, le phénomène devrait\shom{\footnotemark} être représenté par un objet de type ponctuel.

\footnotetext{%
	\shom{Lorsque la déclinaison dans une zone est définie par une fourchette et non par une valeur spécifique, la fourchette de valeurs devrait [doit] être indiquée au moyen d'INFORM (ex : \textit{From -27 degrees to 3 degrees}) \shom{et de l'attribut NINFOM (ex : \textit{De --27 degrés à 3 degrés)}}.
	Lorsque l'anomalie magnétique occupe une surface réduite à l'échelle de compilation (quelques cm\textsuperscript{2}), l'information doit être codée par un \textbf{LOCMAG} de type ponctuel avec INFORM et NINFOM codés, comme par exemple :\newline
	INFORM - \textit{Local magnetic anomaly has been reported in the vicinity of this position.}\newline
	NINFOM - \textit{Une anomalie magnétique locale a été signalée dans le voisinage de cette position.}\newline
	Cependant, un \textbf{LOGMAG} de type surfacique doit être créé si l'anomalie magnétique occupe une surface étendue.\newline
	S'il n'est pas possible de limiter la zone, plusieurs objets de type ponctuel \sout{répartis sur la légende} doivent être créés.}
}

\section{Données sur la marée (cf.~S-4 -- B-406)}

\shom{\textbf{Les informations sur la marée ne doivent pas être codées.}}

L'introduction d'informations sur la marée dans des lots de données d'ENC est optionnelle.

Pour les ports principaux, les services hydrographiques nationaux ou les organismes qu'ils ont autorisés devraient fournir les prédictions.

Pour les ports rattachés, le Service hydrographique approprié ou les organismes qu'ils ont autorisés devraient, lorsque cela est possible, fournir les prédictions. Si de telles prédictions ne sont pas disponibles, le fabricant d'ECDIS doit contacter le service hydrographique national approprié afin d'obtenir des conseils au sujet des meilleures méthodes de prédiction pour les ports rattachés situés dans la zone de responsabilité de ce service.

Il est recommandé que chaque Service hydrographique approprié, ou que chaque organisme qu'il a autorisé, détermine les limites géométriques à l'intérieur desquelles les informations sur la marée sont applicables et le nombre de stations de marée à utiliser dans les calculs de modélisation. Les Services hydrographiques devraient être responsables de la détermination des meilleures méthodologies à appliquer dans leurs zones de responsabilité.

Lorsque des informations sur la marée sont codées, leur niveau de confiance doit être au minimum de 95\%, sans tenir compte de la méthode d'application ou de son origine.

\subsection{Séries chronologiques de données}
\label{T_TIMS}
\index{T\_TIMS}
\textbf{Les informations sur la marée ne doivent pas être codées.}

Si les heures et les hauteurs de pleines et de basses mers doivent être codées, l'objet \textbf{T\_TIMS} doit être utilisé. De plus, lorsque les données sont disponibles, une série chronologique régulière de hauteurs de marées devrait aussi être codée au moyen de cette classe d'objets.

\begin{objet}
	Objet géographique~:& Tide-time series (\textbf{T\_TIMS})  ~ (P, A)\\
	Attributs~:& NOBJNM  ~ OBJNAM\\
	&\underline{TIMEND}  ~ \underline{TIMSTA} - précisent la période pendant laquelle la série est valide\\
	&T\_ACWL  ~ T\_TSVL  ~ T\_TINT  ~ \underline{T\_HWLW}  ~ STATUS  ~ INFORM  ~ NINFOM\\
\end{objet}


\subsection{Prédiction par méthodes harmoniques}
\label{T_HMON}
\index{T\_HMON}
\shom{\textbf{Les informations sur la marée ne doivent pas être codées.}}

Si les paramètres pour la prédiction des hauteurs de marée par les méthodes harmoniques doivent être codés, l'objet \textbf{T\_HMON} doit être utilisé. L'autorité ayant fourni les paramètres devrait être consultée afin de connaître la manière d'utiliser ces données et afin de connaître les algorithmes de calcul à utiliser avec les données.

\begin{objet}
	Objet géographique~:& Tide-harmonic prediction (\textbf{T\_HMON})  ~ (P, A)\\
Attributs~:	& NOBJNM  ~ OBJNAM  ~ T\_ACWL\\
			& \underline{T\_MTOD} - 
				\begin{tabular}{l}
					\textit{1}- méthode harmonique simplifiée pour la prédiction de la marée\\
					\textit{2}- méthode harmonique complète pour la prédiction de la marée\\
				\end{tabular} \\
			&\underline{T\_VAHC}  ~ STATUS  ~ INFORM  ~ NINFOM\\
\end{objet}

\subsection{Prédiction par méthodes non harmoniques}
\label{T_NHMN}
\index{T\_NHMN}
\shom{\textbf{Les informations sur la marée ne doivent pas être codées.}}

Si les paramètres pour la prédiction des hauteurs de marée par l'utilisation des différences d'heures et de hauteurs doivent être codés, l'objet \textbf{T\_NHMN} doit être utilisé.

Le port de référence à utiliser pour ces prédictions doit être identifié par un objet collection \textbf{C\_ASSO} qui associe l'objet \textbf{T\_TIMS} ou \textbf{T\_HMON} donnant les paramètres de marée du port de référence et le \textbf{T\_NHMN} du port rattaché. Si le port de référence n'est pas situé à l'intérieur de la cellule ou du lot d'échange, alors ses paramètres de marée devraient être fournis par un objet géographique sans aucune référence géométrique.

Actuellement, le codage des autres méthodes non harmoniques pour la prédiction des hauteurs de marée n'est pas prévu.


\begin{objet}
Objet géographique~:& Tide-non-harmonic prediction (\textbf{T\_NHMN})  ~ (P, A)\\
Attributs~:	& NOBJNM  ~ OBJNAM  ~ T\_ACWL\\
			&\underline{T\_MTOD} - ~ \textit{3} - méthode non harmonique des différences d'heures et de hauteurs\\
			&\underline{T\_THDF}  ~ STATUS  ~ INFORM  ~ NINFOM\\
\end{objet}

\section{Données sur les courants de marée (cf.~S-4 - B-407)}

\subsection{Courant de marée (flot/jusant)}
\label{TS_FEB}
\index{TS\_FEB}
Si une information sur le courant de marée limitée seulement aux directions de flot et de jusant et/ou à leurs valeurs doit être codée, l'objet \textbf{TS\_FEB} doit être utilisé.

\begin{objet}
	Objet géographique~:& Tidal stream - flood/ebb (\textbf{TS\_FEB}) (P, A)\\
Attributs~:& \underline{CAT\_TS} -\\
&\underline{CURVEL} - vitesse maximale (en vive-eau)\\
&DATEND  ~ DATSTA ~ NOBJNM ~ OBJNAM ~ \underline{ORIENT} ~ PEREND ~ PERSTA  ~ INFORM ~ NINFOM\\
\end{objet}


\subsection{Séries chronologiques de données sur les courants de marée}
\label{TS_TIS}
\index{TS\_TIS}
\shom{\textbf{L'objet TS\_TIS ne doit pas être utilisé.}}

Si des séries chronologiques de données sur les courants de marée doivent être codées, l'objet \textbf{TS\_TIS} doit être utilisé.

\begin{objet}
	Objet géographique~:& Tidal stream - time series (\textbf{TS\_TIS}) (P, A)\\
Attributs~:& NOBJNM ~ OBJNAM\\
	&\underline{TIMEND} ~ \underline{TIMSTA} - précisent la période pendant laquelle la série est valide\\
	&\underline{TS\_TSV} ~ \underline{T\_TINT} ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\subsection{Prédiction par méthodes harmoniques}
\label{TS_PRH}
\index{TS\_PRH}
\shom{\textbf{L'objet TS\_PRH ne doit pas être utilisé.}}

Si les paramètres pour la prédiction des courants de marée par les méthodes harmoniques doivent être codés, l'objet \textbf{TS\_PRH} doit être utilisé. L'autorité ayant fourni les paramètres devrait être consultée afin de connaître la manière d'utiliser ces données et afin de connaître les algorithmes de calcul à utiliser avec les données.


\begin{objet}
Objet géographique~:& Tidal stream-harmonic prediction (\textbf{TS\_PRH}) ~ (P, A)\\
Attributs~:& NOBJNM ~ OBJNAM\\
			&\underline{T\_MTOD} ~ - 
				\begin{tabular}{l}
					\textit{1} - méthode harmonique simplifiée pour la prédiction de la marée\\
					\textit{2} - méthode harmonique complète pour la prédiction de la marée\\
				\end{tabular}\\
			&\underline{T\_VAHC} ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}


\subsection{Prédiction par méthodes non harmoniques}
\label{TS_PNH}
\index{TS\_PNH}
\shom{\textbf{L'objet TS\_PNH ne doit pas être utilisé.}}

Si les paramètres pour la prédiction des courants de marée par l'utilisation des différences d'heures et de vitesses doivent être codés, l'objet \textbf{TS\_PNH} doit être utilisé.

La station de référence pour ces prédictions doit être identifiée par un objet collection \textbf{C\_ASSO} qui associe l'objet \textbf{TS\_TIS} ou \textbf{TS\_PRH} donnant les paramètres de courant de la station de référence et le \textbf{TS\_PNH} de la station secondaire. Si la station de référence n'est pas située à l'intérieur de la cellule ou du lot d'échange, alors ses paramètres de courants de marée devraient être fournis par un objet géographique sans aucune référence géométrique.

Actuellement, le codage des autres méthodes non harmoniques pour la prédiction des courants de marée n'est pas prévu.

\begin{objet}
		Objet géographique~:& Tidal stream - non-harmonic prediction (\textbf{TS\_PNH}) ~ (P, A)\\
Attributs~:& NOBJNM ~ OBJNAM ~ \underline{T\_THDF}\\
&\underline{T\_MTOD} - \textit{3} - méthode non harmonique des différences d'heures et de hauteurs\\
&STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\subsection{Tableaux de courants de marée (cf.~S-4 -- B-407.3)}
\label{TS_PAD}
\index{TS\_PAD}
Si l'information généralement indiquée sur les cartes papier sous la forme d'un tableau de stations doit être codée, l'objet \textbf{TS\_PAD} doit être utilisé.

Les valeurs des courants de marée ainsi codées doivent être des vitesses en vives-eaux moyennes, c'est-à-dire~les vitesses des courants de marée associées à un marnage défini comme étant la différence de hauteur entre la PMVE et la BMVE.

\begin{tabular}{ll}
Objet géographique~:& Tidal steam panel data (\textbf{TS\_PAD}) ~ (P, A)\\
Attributs~:& NOBJNM  ~ OBJNAM ~ \underline{TS\_TSP} ~ INFORM ~ NINFOM\\

\end{tabular}
\subsection{Courants généraux (cf.~S-4 -- B-408)}
\label{CURENT}
\index{CURENT}
Si les courants généraux (non gravitationnels) (en particulier les courants océaniques) doivent être codés, l'objet \textbf{CURENT} doit être utilisé.

\begin{objet}
Objet géographique~:& Current (\textbf{CURENT}) ~ (P)\\
Attributs~:	& \underline{CURVEL} ~ DATEND ~ DATSTA ~ NOBJNM ~ OBJNAM ~ \underline{ORIENT} ~ PEREND ~ PERSTA ~ INFORM ~ NINFOM\\
\end{objet}


\end{document}