\documentclass[../Main.tex]{subfiles}
\begin{document}


Si un nom géographique international ou national doit être codé, les attributs OBJNAM et NOBJNM\textsuperscript{(1)} doivent être utilisés tel que cela est précisé dans la Spécification de Produit des ENC (S-57 Appendice B.1), § 3.11. L'utilisation des objets cartographiques \$TEXTS est interdite.

Lorsque cela est possible, les objets existants (ex : \textbf{BUAARE}, \textbf{RIVERS}, marques de navigation) devraient \shom{[doivent]} être utilisés pour coder cette information.

Si un nom géographique doit être codé et pour lequel il n'y a pas d'objet existant, un objet spécifique \textbf{ADMARE}, \textbf{SEAARE} ou \textbf{LNDRGN} doit être créé (cf.~§ 4.7.1 et 8). Afin de réduire le volume des données, ces objets devraient \shom{[doivent]}, dans la mesure du possible, utiliser la géométrie d'autres objets existants, par exemple~:~un objet \textbf{SEAARE} peut utiliser la géométrie d'un objet \textbf{DEPARE}.

Les noms géographiques nationaux peuvent être soit maintenus dans leur langue nationale originelle dans l'attribut international OBJNAM (mais seulement si la langue nationale peut s'exprimer avec le niveau lexical 0 ou 1), soit translittérés ou transcrits et codés par l'attribut international OBJNAM, auquel cas le nom national devrait être renseigné dans l'attribut national NOBJNM \shom{(cf.~14 à 18 ci-dessous)}.

Les noms géographiques jugés utiles par l'autorité productrice devraient \shom{[doivent]} être codés avec OBJNAM en se basant sur les critères suivants :

\begin{enumerate}[label=\arabic*)]

\item Une pointe ou un cap nommé sur la carte et ne comportant pas d'aide à la navigation devrait \shom{[doit]} être codé par un objet \textbf{LNDRGN} (de type surfacique ou ponctuel) \shom{(voir règles 13 et 14 ci-après)}, avec le nom géographique codé dans OBJNAM.
\item Une pointe ou un cap nommé sur la carte et comportant une d'aide à la navigation devrait \shom{[doit]} être codé par OBJNAM sur l'objet maître associé à l'aide à la navigation; si plusieurs aides à la navigation sont situées sur la pointe ou le cap, un objet \textbf{LNDRGN} (de type surfacique ou ponctuel) devrait \shom{[doit]} être créé et le toponyme codé avec OBJNAM de cet objet \shom{(voir aussi règle 9 ci-après)}.
\item Un groupe d'objets hydrographique (ex~: \textbf{SBDARE}, \textbf{UWTROC}, \textbf{OBSTRN}) associés à un nom géographique particulier, devrait \shom{[doit]} avoir le nom codé dans OBJNAM du \textbf{SEAARE} (de type surfacique ou ponctuel) \shom{(voir ci-après)}. Le toponyme ne devrait pas [ne doit pas] être codé sur les objets hydrographiques individuels.
\item Le nom d'une île importante proche des couloirs de navigation devrait \shom{[doit]} être codé au moyen de OBJNAM de l'objet \textbf{LNDARE} délimitant l'île. Un groupe d'îles associées à un nom géographique devrait \shom{[doit]} avoir le toponyme codé dans OBJNAM d'un objet \textbf{LNDRGN} (de type surfacique ou ponctuel).
\item Les éléments nommés figurant dans les Instructions Nautiques des Services Hydrographiques pouvant faciliter la navigation devraient \shom{[doivent]} être codés par OBJNAM de l'objet approprié (\textbf{LNDRGN}, \textbf{UWTROC}, \textbf{SBDARE}, \textbf{SEAARE}, \textbf{OBSTRN}).
\item Si une zone administrative de juridiction internationale, nationale, municipale ou provinciale pouvant avoir une implication légale doit être codée, un objet \textbf{ADMARE} doit être utilisé, avec le toponyme codé dans OBJNAM.
\item Si une ville importante située le long d'une côte doit être codée, un objet \textbf{BUAARE} ou \textbf{ADMARE} doit être utilisé (cf.~§ 4.8.14), avec le toponyme codé dans OBJNAM.
\item Si le nom d'une rivière, d'un lac ou d'un canal navigable doit être codé, un \textbf{SEAARE} doit être utilisé, avec le toponyme codé dans OBJNAM.
\begin{shom_p}
\item Les noms de plages doivent être, dès que possible, codés sur les objets \textbf{COALNE}.
\item Lorsque deux toponymes maritimes ou terrestres sont présents pour un même lieu géographique, la forme officielle ou la forme anglaise doit être codée dans OBJNAM (exemple : Djazâir), la forme française (exonyme français) doit être codée dans NOBJNM (exemple : Algérie). Si deux toponymes concernant un même lieu d'une région frontière figurent sur la carte papier équivalente, ils doivent être codés dans OBJNAM. On les classe selon l'ordre suivant : forme internationalement admise, forme française, forme anglaise, autre forme. La seconde forme de ce classement est écrite entre parenthèses.
\item Lorsqu'un toponyme maritime est figuré uniquement dans une forme étrangère, il doit être codé dans OBJNAM.
\item Lorsqu'un toponyme maritime est figuré en français,
	\begin{itemize}
	\item s'il est situé dans une zone de souveraineté française (ou de souveraineté étrangère dont le français est la langue officielle), il doit être codé dans OBJNAM conformément à la carte papier équivalente,
	
	\item s'il ne remplit pas la condition précédente, il doit être codé dans NOBJNM conformément à la carte papier équivalente, et aussi dans OBJNAM dans sa forme anglaise (prise dans la publication B-8 de l'OHI, ou à défaut sur la carte britannique d'échelle équivalente à celle de la carte papier équivalente).
	\end{itemize}

\item Si un objet \textbf{ADMARE} (cf.~§ 11.2) \textbf{doit être créé pour} coder un pays identifié sur la carte papier équivalente par un toponyme, le codage doit être adapté aux règles de la norme S-52 pour que ce toponyme important soit affiché sur l'ECDIS.

Pour cela, dans la plupart des cas, un objet additionnel \textbf{LNDRGN} ponctuel doit être créé à la position du toponyme mentionné. L'attribut OBJNAM de l'objet \textbf{LNDRGN} est codé par le toponyme, l'attribut OBJNAM de l'objet \textbf{ADMARE} n'est pas codé.

Cependant, si la géométrie de la partie représentée du pays coïncide avec un seul objet surfacique \textbf{LNDARE}, le toponyme mentionné est codé dans OBJNAM de l'objet \textbf{LNDARE}, l'attribut OBJNAM de l'objet \textbf{ADMARE} n'est pas codé. Aucun objet additionnel \textbf{LNDRGN} n'est créé.

\item Le nom des sommets du relief terrestre sera généralement codé par l'attribut OBJNAM sur l'objet \textbf{LNDELV} existant (sans coder d'objet additionnel \textbf{LNDRGN}).

Si le sommet est mis en évidence sur la carte marine (écrit en capitales, ou en gras...) ou s'il est cité dans les Instructions Nautiques, ou bien s'il marque un relèvement, on codera OBJNAM sur le \textbf{LNDMRK} figurant l'amer, ou sur un objet additionnel \textbf{LNDRGN} créé au même point que le \textbf{LNDELV}, pour rendre visible le nom du sommet et son élévation.
\end{shom_p}
\end{enumerate}

\begin{shom_p}
	\textbf{Géométrie :}

\begin{enumerate}[label=\arabic*),resume]

\item Si l'écriture d'un toponyme sur la carte papier équivalente est à position, elle doit être codée par un \textbf{SEAARE}/\textbf{LNDRGN} ponctuel (ex : petite baie, pointe).
\item Si l'écriture d'un toponyme sur la carte papier équivalente est à disposition, elle doit être codée par un \textbf{SEAARE}/\textbf{LNDRGN} de type surfacique. Dans ce cas, la géométrie d'une pointe ou d'une baie doit, si possible, s'appuyer sur le trait de côte.

\end{enumerate}
\end{shom_p}

Dans tous les cas, si l'étendue exacte de l'élément à nommer est connue, un objet de type surfacique doit être créé. Si l'étendue exacte n'est pas connue, un objet de type ponctuel, existant ou spécifiquement créé, devrait \shom{[doit]} être utilisé pour coder le toponyme.

\begin{quote}

\end{quote}

\section{\texorpdfstring{\shom{Noms des structures (supports d'aides à la navigation, autres édifices)}}{Noms des structures (supports d'aides à la navigation, autres édifices)}}
\begin{shom_p}

Le nom d'une structure doit être codé dans OBJNAM seulement (ex~: OBJNAM = \textit{Parquette}). Les termes génériques (ex : phare, colonne, tour), lorsqu'ils sont conservés dans le nom, doivent être en français.

Excepté s'il s'agit d'un numéro ou d'une abréviation (ex : CA2, No6), le nom d'une structure doit être codé en caractères minuscules précédés d'une majuscule (que la structure soit ou non traitée en amer remarquable).

Exemples de codage de noms de structures

\begin{longtable}[c]{|l|l|}
	\hline
	\rowcolor[HTML]{C0C0C0} 
	\textbf{Nom}                                            & \textbf{OBJNAM sur l'objet structure} \\ \hline
	\endhead
	%
	Bouée ZC1 (ZC1 est peint sur la structure)              & ZC1                                   \\ \hline
	Bouée No5 (le chiffre 5 est peint sur la   structure)   & No5                                   \\ \hline
	Phare de Nividic (NIVIDIC est peint sur la   structure) & Nividic                               \\ \hline
	Phare de la Jument                                      & La Jument                             \\ \hline
	Phare de Calais                                         & Phare de Calais (*)                   \\ \hline
	Eglise de Plouarzel                                     & Plouarzel                             \\ \hline
	Eglise Saint-Martin                                     & Saint-Martin                          \\ \hline
	Colonne de la Grande Armée                              & Colonne de la Grande Armée            \\ \hline
	\caption{codage de noms de structures}
	\label{tab:14.1}\\
\end{longtable}

Le nom de la structure doit être codé si et seulement si au moins une des conditions suivantes est remplie~:

\begin{enumerate}[label=\arabic*.]
\item Si le nom de l'objet est figuré comme tel sur la carte papier équivalente. Il peut être directement associé au symbole ou mentionné sur la légende d'un alignement ou d'un relèvement. Un nom d'objet peint sur la structure est normalement figuré sur la carte papier équivalente ;
\item Si l'objet est une aide à la navigation, lorsqu'il est situé en un lieu (ex : île, pointe, roche) dont le toponyme associé est figuré sur la carte  papier équivalente et que ce toponyme est aussi le nom de l'aide à la navigation ;
\item Si l'objet est un amer marquant une agglomération située à l'intérieur des terres et que l'agglomération elle-même n'est pas codée (ex : pour l'objet \textbf{LNDMRK} codant l'église de Plouarzel, OBJNAM = \textit{Plouarzel}) ;
\item Si le nom de l'objet (notoirement connu) est cité dans les Instructions Nautiques (normalement, le nom est aussi figuré sur la carte  papier équivalente ou sur une des cartes papier à plus grande échelle). Dans ce cas, le nom de la structure doit être identifié par le SHOM sur le support d'aide à la codification.
\end{enumerate}

\end{shom_p}
\end{document}