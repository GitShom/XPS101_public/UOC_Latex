\documentclass[../Main.tex]{subfiles}
\begin{document}

\section{Niveau de référence des sondes}

Cf. § 2.1.3.

\section{Isobathes (cf.~S-4 -- B-411)}
\label{DEPCNT}
\index{DEPCNT}

\begin{objet}
	Objet géographique~:& Depth contour (\textbf{DEPCNT}) ~ (L)\\

	Attributs~:& \underline{VALDCO} - cote de l'isobathe (valeur négative si la courbe de niveau est découvrante).\\
	&\sout{VERDAT} ~ INFORM ~ NINFOM\\

\end{objet}

La limite d'une zone rocheuse qui couvre et découvre (cf.~INT 1 - \shom{J}20) ou d'un récif corallien (cf.~INT 1 - J22) peut coïncider avec la laisse de basse mer (cf.~"fg" en Figure \ref{fig:image4}). Si cette limite doit être codée, l'objet \textbf{DEPCNT} doit être utilisé, avec VALDCO~=~\textit{0}.

Sur les cartes papier, la représentation des isobathes dans des zones de forte pente est parfois généralisée de façon à ne conserver qu'une seule courbe parmi plusieurs très proches (cf.~"ab" en Figure \ref{fig:image4}). Dans ce cas, l'isobathe est codée avec la plus petite des profondeurs.

Lorsque cela est possible, les courbes doivent être fermées et connectées avec la bordure de la cellule, le trait de côte ou avec d'autres courbes afin de définir des surfaces fermées.

\shom{Si exceptionnellement, il s'avère nécessaire de compléter une isobathe sur une longueur supérieure à trois millimètres, les objets spatiaux associés doivent être codées avec QUAPOS\textit{~=~4} (position approximative).}

Les objets spatiaux associés à des isobathes approchées (cf.~S-4 - B-411.2) doivent être codées avec QUAPOS\textit{~=~4} (approximatif).

\begin{figure}[H]
	\centering
	\includegraphics[height=0.5\textheight]{sections/media/image4}
	\caption[Isobathes]{Isobathes}
	\label{fig:image4}
\end{figure}


Remarques~:

\begin{itemize}
\item Les Autorités productrices devraient, au minimum, utiliser les isobathes normalisées (cf.~S-4 -- B-411). Des
isobathes supplémentaires peuvent être utilisées si nécessaire.

\end{itemize}

\section{Sondes (cf.~S-4 -- B-412 et B-413.1)}
\label{SOUNDG}
\index{SOUNDG}

\begin{objet}
Objet géographique~:& Sounding (\textbf{SOUNDG}) ~ (P)\\
Attributs~:& EXPSOU - indique si les "valeurs de sondes" de l'objet sont comprises ou non dans la gamme de profondeur de l'objet de type surfacique sous-jacent (mais voir la \textbf{NOTE} ci-dessous).\\
&NOBJNM ~ OBJNAM\\
&QUASOU - voir le tableau 5.1 ci-après\\
&SOUACC - voir utilisation de \textbf{M\_QUAL} (cf.~§ 2.2.3.1 and § 2.2.3.4)\\
&STATUS - \textit{18} - si existence douteuse\\
&TECSOU - seulement pour les sondes de fiabilité moindre\\
&\sout{VERDAT} ~ INFORM ~ NINFOM\\
&SORDAT - voir le tableau \ref{tab:5.1} ci-après.\\
\end{objet}

\begin{longtable}[]{|L{0.15\linewidth}|C{0.1\linewidth}|C{0.1\linewidth}|C{0.1\linewidth}|C{0.1\linewidth}|L{0.23\linewidth}|}
	\hline
	\rowcolor[HTML]{C0C0C0}
	\textbf{Sonde} & \textbf{S-4} & \textbf{INT 1} & \textbf{QUAPOS} & \textbf{QUASOU} & \textbf{Commentaire} \\
	\endfirsthead
	%
	\hline
	\rowcolor[HTML]{C0C0C0}
	\textbf{Sonde} & \textbf{S-4} & \textbf{INT 1} & \textbf{QUAPOS} & \textbf{QUASOU} & \textbf{Commentaire} \\
	\endhead
	%
	en position réelle (profondeur connue) & B-412.1 & I10 &  & 1 & Devrait \shom{[ne doit pas]} être codé avec QUAPOS = 10 \\ \hline

	hors position sur la carte papier (profondeur connue)& B-412.2  & I11 / I12 & 1 &  & L'objet spatial doit être codé à la position réelle; il n'y a pas de sondes "hors position" dans une ENC. \\ \hline

	pas trouvé le fond & B-412.3 & I13 &  & 5 & Pour les sondes «pas trouvé le fond», lorsque la valeur de la sonde est inférieure à celles de la gamme des profondeurs environnantes, la valeur EXPSOU = 2 (moins profond que la gamme des profondeurs environnantes) devrait \shom{[doit]} être renseignée (exception admise à la NOTE ci-dessous). \\ \hline

	incertaine & B-412.4 & I14 & 4 & 4 &  \\ \hline

	découvrante (profondeur connue) & B-413.2 & I15 &  & 1 & Valeur de VALSOU négative \\ \hline

	douteuse & B-424.4 & I2 &  & 3 & L'existence douteuse devrait \shom{[doit]} être codée par STATUS = 18 \\ \hline

	signalée mais non confirmée & B-242.5 C-404.3 & I3 / I4 & 8 & 9 & L'année du renseignement, si elle est disponible, doit être codée par SORDAT (cf. § 2.1.5) \\ \hline

	\caption{SORDAT}
	\label{tab:5.1}\\
\end{longtable}

Une sonde associée à une tête de roche ou de corail et qui constitue une obstruction pour la navigation (cf.~INT 1 - K14) doit être codée\footnoteshom{Pour le codage des roches qui peuvent couvrir, voir § 6.1.2.} par un \textbf{UWTROC} avec VALSOU renseigné par la valeur de la sonde.

L'objet spatial est une matrice à trois colonnes (latitude, longitude, profondeur). Dans un but d'efficacité, plusieurs sondes devraient \shom{[doivent]} être codées par un même objet spatial, à condition que les valeurs de tous les attributs de l'objet spatial et de l'objet géographique soit communes au groupe.

Comme le facteur multiplicatif des sondes (SOMF) pour les ENC est toujours 10, les sondes doivent toujours être codées en décimètres. Les sondes découvrantes doivent être codées avec des valeurs négatives.

Pour les sondes entourées d'une ligne de danger, voir § 6.3.

\textbf{NOTE}~: L'attribut EXPSOU indique si la «valeur de la sonde» est située, ou non, dans la gamme des profondeurs de l'objet \textbf{DEPARE} ou \textbf{DRGARE} environnant. Cela permet de coder dans une ENC un objet \textbf{SOUNDG} ayant une «~valeur de sonde~» inférieure à celles de l'objet \textbf{DEPARE} ou \textbf{DRGARE} dans lequel il est situé.
La classe d'objet \textbf{SOUNDG} n'est pas dans la liste des informations du SENC qui doivent être affichées sur l'ECDIS soit dans le mode «~Affichage de Base~» ou soit dans le mode «~Affichage Standard~», sauf si l'opérateur le demande par le menu sélection. Par conséquent, les sondes inférieures à la profondeur de sécurité d'un navire, telle qu'elle est définie dans l'ECDIS, ne seront pas affichées en mode «~Affichage de Base~» ou en mode «~Affichage Standard~». De plus, il n'existe aucune garantie que le système anti-échouage de l'ECDIS détectera de telles sondes soit dans le mode de planification de route, ou soit dans dans le mode de suivi de route. Il peut en résulter qu'un danger potentiel pour la navigation ne soit détecté ni par le navigateur ni par le système utilisé.

Par conséquent, il est fortement recommandé de ne pas utiliser la valeur d'attribut EXPSOU = \textit{2} (moins profond que la gamme des profondeurs environnantes) pour les objets \textbf{SOUNDG}. Quand une sonde moins profonde que celles de la gamme de profondeurs de l'objet \textbf{DEPARE} ou \textbf{DRGARE} environnant est trouvée, il est fortement conseillé de mener une recherche complémentaire dans les documents source afin de coder des isobathes et des informations bathymétriques additionnelles plus cohérentes avec la sonde, ou bien d'utiliser l'attribut DRVAL2 pour indiquer la profondeur théorique de dragage et DRVAL1 pour coder la plus courte des sondes figurées dans la zone draguée. Il est également possible d'envisager l'utilisation d'une classe d'objets autre que \textbf{SOUNDG} (ex~: \textbf{OBSTRN -} cf.~§ 6.2.2) pour coder la profondeur. Pour le codage des remontées de fond dans des zones draguées, voir le § 5.5.

\shom{Par ailleurs, EXPSOU ne doit pas être renseigné pour les sondes codées à l'intérieur d'un \textbf{UNSARE}.}


\section{Zones de profondeurs}

\subsection{Objet géographique "Depth area"}
\label{DEPARE}
\index{DEPARE}
La zone maritime, l'estran et les parties navigables des rivières, des lacs et des canaux doivent être divisées en zones de profondeurs, chacune d'elles correspondant à une gamme de profondeurs.

Autant d'objets "Depth area" que possible doivent être créés à partir des isobathes codées.

\shom{Il n'y a désormais plus aucune exigence de coder les \textbf{DEPARE} de type linéaire dans les ENC.}

Les \textbf{DEPARE} de type linéaire dans les lots de données existants doivent être considérés comme des données redondantes \shom{et doivent être supprimées} lors de l'édition suivante de l'ENC.

Les \textbf{DEPARE} de type surfacique font partie du Groupe 1.

\begin{objet}
	Objet géographique~:& Depth area (\textbf{DEPARE}) ~ (A)\\

Attributs~:& \underline{DRVAL1} - (cf.~§ 5.4.3)\\

&\underline{DRVAL2} - (cf.~§ 5.4.3)\\

&QUASOU ~ \sout{SOUACC} ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
\end{objet}

\subsection{Géométrie des zones de profondeurs}

Lorsque des zones ne sont pas fermées sur le document papier équivalente, il peut être nécessaire de fermer ces zones par des arcs sans aucun objet de type linéaire associé. Ceci est obligatoire sur les limites d'une cellule (voir Figure 5).

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{sections/media/fig5a}
	\caption[Géométrie des zones de profondeurs]{Géométrie des zones de profondeurs}
	\label{fig:fig5a}
\end{figure}

Remarque :

Pour des portions de \textbf{DEPCNT} courtes et isolées telles que (bc), c'est à l'autorité productrice de décider si elle doit coder la petite surface (abcda) par un \textbf{DEPARE} de type surfacique distinct ou coder seulement la ligne (bc) comme un objet \textbf{DEPCNT} de type linéaire "flottant" à l'intérieur d'un seul \textbf{DEPARE} ayant pour attributs DRVAL1 = \textit{5} et DRVAL2 = \textit{20}.

\subsection{Utilisation des attributs DRVAL1 et de DRVAL2 pour les zones de profondeurs en général}

Pour les \textbf{DEPARE} de type surfacique, DRVAL1 et DRVAL2 devraient \shom{[doivent]} être renseignés avec les valeurs correspondant aux profondeurs minimales et maximales trouvées dans cette zone. Ces valeurs, sauf pour les zones de profondeurs minimale et maximale, devraient \shom{[doivent]} être choisies parmi les cotes des isobathes codées dans le lot de données.

Une zone d'estran dans laquelle une sonde découvrante sans position réelle est indiquée devrait \shom{[doit]} être codée par un \textbf{DEPARE} avec DRVAL1 renseigné par la valeur de la cote d'estran et DRVAL2 par une valeur d'isobathe du lot de données (habituellement zéro). Une alternative est de renseigné DRVAL1 par -H (voir NOTE (a) associée à la Figure 6 ci-dessous pour la définition de H) et avec la hauteur de l'estran renseignée par l'attribut INFORM de la \textbf{DEPARE}. La sonde découvrante doit être codée par INFORM (et NINFOM) sur l'objet \textbf{DEPARE} (ex : INFORM = \textit{Dries 1,4}~; NINFOM = \textit{1,4 mètre au-dessus du zéro des sondes}).

Si une zone de profondeurs est adjacente à une zone non-navigable, une ligne de fermeture (c'est-à-dire sans objet géographique linéaire) devrait \shom{[doit]} être codée à la limite entre les zones navigables et non navigables. Voir § 5.4.8.

\begin{shom_p}
	Remarques :

Dans les zones côtières, il arrive parfois que localement les isobathes ne soient pas figurées pour toutes les valeurs standards. Si la carte est à grande échelle et si la zone concernée a été correctement hydrographiée (sources fiables, sondes et isobathes précises\ldots), il ne faut pas coder l'information bathymétrique de façon trop pessimiste.
Par exemple, si une isobathe de 10 mètres vient localement se confondre avec le trait de côte et \underline{que toutes les sondes figurées sont supérieures à cette profondeur}, on doit considérer que le rivage est accore. L'objet \textbf{DEPARE} de type surfacique adjacent au trait de côte doit être codé avec DRVAL1 = 10, et non avec DRVAL1 = 0.

Lorsque la valeur de l'attribut DRVAL2 des zones de profondeurs maximales n'est pas déterminable, la valeur de l'isobathe standard immédiatement supérieure à la plus profonde des valeurs d'isobathes et des sondes figurées sur la carte doit être retenue.
\end{shom_p}


\begin{figure}[H]
	\centering
	\def\svgwidth{\linewidth}
	\input{sections/media/Fig-5.4.3.pdf_tex}
	\caption[Zones de profondeur]{Zones de profondeur}
	\label{fig:5.4.3}
\end{figure}


\textbf{NOTE} \textbf{(a)} : H = cote du niveau du trait de côte au-dessus du niveau papier équivalente des sondes, ou valeur arrondie (exemples : (1) - la cote de la plus haute des isobathes découvrantes figurées sur le document source \shom{(carte papier équivalente)} ou (2) - zéro, si le niveau du trait de côte est aussi le niveau papier équivalente des sondes).

\shom{La valeur de H est définie par le SHOM et doit être indiquée dans le cahier de définition. Elle est constante dans une même cellule et est déduite du niveau moyen à l'intérieur de la cellule (H = 2 niveau moyen, puis arrondi au mètre supérieur). La valeur de H doit être nulle (H = \textit{0}) si aucune zone d'estran n'est figurée sur la carte papier équivalente dans la zone couverte par la cellule (ex :~zone à faible marnage, carte à petite échelle, etc.).}

Les numéros des sous-paragraphes ci-après se réfèrent aux numéros des différents cas illustrés en Figure \ref{fig:5.4.3}. Ces sous-paragraphes ne couvrent pas l'ensemble des scénarios de codage possibles.

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item Si la zone de profondeur est limitée par deux ou plus de deux isobathes :

	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe du lot de données immédiatement inférieure à la valeur de DRVAL2.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de la plus profonde des isobathes limitant la zone.
	\end{itemize}

\item Si la plus grande profondeur est figurée par une isobathe, et si la plus petite est figurée par une sonde (un haut-fond) :

	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre soit la valeur de la cote de l'isobathe du lot de données immédiatement inférieure à la valeur de la sonde, soit -H.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe.
	\end{itemize}

\item Si la plus grande profondeur est figurée par une sonde, et si la plus petite est figurée par une isobathe (un creux) :


	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe du lot de données immédiatement supérieure ou égale à la valeur de la sonde.
	\end{itemize}

\item Si la plus petite profondeur est figurée par le trait de côte~:

	\begin{itemize}
	\item DRVAL1~devrait \shom{[doit]} prendre la valeur -H.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de la plus profonde des isobathes limitant la zone.
	\end{itemize}

\item Si la zone est limitée par une seule isobathe, ne contient pas de sonde et marque un haut-fond~:

	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre soit la valeur de la cote de l'isobathe du lot de données immédiatement inférieure à celle de l'isobathe représentée, soit -H.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe représentée.
	\end{itemize}

\item Si la zone est limitée par une seule isobathe, ne contient pas de sonde et marque un creux~:

	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe représentée.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe du lot de données immédiatement supérieure à celle de l'isobathe représentée.
	\end{itemize}

\item Si la zone de profondeur est limitée par une isobathe incomplète d'un côté (par exemple dans une zone incomplètement hydrographiée), et par une isobathe complète de l'autre~:

Le codage de ce \textbf{DEPARE} de type surfacique est optionnel, voir § 4.5.2 et Figure \ref{fig:fig5a}.

\item Si la zone de profondeur est limitée par des isobathes complètes mais contient une isobathe incomplète~(flottante) :

	\begin{itemize}
	\item DRVAL1 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe la moins profonde.
	\item DRVAL2 devrait \shom{[doit]} prendre la valeur de la cote de l'isobathe la plus profonde.
	\end{itemize}


NOTA~: Ce codage est obligatoire, que la \textbf{DEPARE} optionnelle du sous-paragraphe 7 ci-dessus soit codé ou pas.

\end{enumerate}


\subsection{N'est pas utilisé}

\subsection{N'est pas utilisé}

\subsection{N'est pas utilisé}

\subsection{N'est pas utilisé}

\subsection{Rivières, canaux, lacs et bassins}
Lorsque ces zones sont navigables à l'échelle de compilation, elles doivent être codées par des objets du Groupe~1 (\textbf{DEPARE}, \textbf{DRGARE} ou \textbf{UNSARE}) et des objets de type trait de côte (\textbf{COALNE} ou \textbf{SLCONS}). Si la nature ou le nom de la zone doit être codée, l'objet \textbf{SEAARE} doit être utilisé.

Lorsque ces zones ne sont pas navigables à l'échelle de compilation, elles doivent être décrites par des objets \textbf{RIVERS}, \textbf{CANALS}, \textbf{LAKARE}, \textbf{DOCARE} ou \textbf{LOKBSN}.
Ces objets doivent être couverts par des \textbf{LNDARE} ou \textbf{UNSARE}\footnoteshom{Un \textbf{UNSARE} doit être utilisé.}. L'utilisation des classes d'objet \textbf{CANBNK}, \textbf{LAKSHR} et \textbf{RIVBNK} est interdite.

\section{Zones draguées (cf.~S-4 -- B-414)}
\label{DRGARE}
\index{DRGARE}
Si une zone draguée doit être codée, l'objet \textbf{DRGARE} doit être utilisé.


\begin{objet}
		Objet géographique :& Dredged area (\textbf{DRGARE}) ~ (A)\\
Attributs~:& \underline{DRVAL1} - profondeur de dragage\\
&DRVAL2 - profondeur de dragage (si différent de DRVAL1)\\
&NOBJNM ~ OBJNAM\\
&QUASOU -
	\begin{tabular}{|L{0.75\linewidth}}
	\textit{10 -} profondeur entretenue\\
	\textit{11 -} pas régulièrement entretenue\\
	Si renseignée, la valeur de QUASOU doit être une des deux ci-dessus.\\
	\end{tabular}\\
&RESTRN\\
&SOUACC - voir utilisation de \textbf{M\_QUAL} (§ 2.2.3.1)\\
&TECSOU ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
&SORDAT - date du dragage ou du dernier levé de contrôle\\
\end{objet}


Remarques~:

Les objets \textbf{DRGARE} de type surfacique font partie du Groupe 1.

Les limites de zones draguées ne devraient \shom{[doivent]} pas coïncider avec des objets géographiques linéaires, excepté les portions de la limite qui correspondent au trait de côte (§ 4.5).

Les zones draguées sont souvent sujettes à envasement, et cela fait que des sondes inférieures à la profondeur de dragage théorique peuvent y être trouvées. Quand un objet \textbf{SOUNDG} est codé dans une zone draguée pour indiquer des profondeurs inférieures à la profondeur de dragage, la valeur d'attribut EXPSOU = \textit{2} (moins profond que la gamme des profondeurs environnantes) ne doit pas être codée (cf.~§ 5.3).
Lorsque ces profondeurs doivent être codées, l'objet \textbf{SOUNDG} devrait être utilisé avec les informations appropriées sur le fond (\textbf{DEPCNT} et \textbf{DEPARE}) cohérentes avec les profondeurs.
Comme autre solution possible, l'attribut DRVAL2 du \textbf{DRGARE} peut être codé avec la valeur de la profondeur de dragage théorique de la zone draguée, et l'attribut DRVAL1 avec la valeur de la plus petite des prodondeurs. Lorsque les sondes inférieures à la profondeur de dragage sont proches de la limite de la zone draguée, celle-ci peut être ajustée afin d'exclure ces sondes de la zone.

L'attribut SORDAT peut \shom{[doit]} être utilisé pour coder l'année du dernier levé de contrôle des zones draguées dont la profondeur de dragage n'est pas maintenue. Pour les zones draguées dont la profondeur de dragage est maintenue, il n'est pas nécessaire d'indiquer l'année de dragage.

\begin{shom_p}
	Sur les cartes qui ne sont pas à plus grande échelle, des zones draguées peuvent être figurées sans aucune profondeur de dragage, alors que la bathymétrie environnante est entièrement ou partiellement représentée. Chacune de ces zones doit être codée par un \textbf{DRGARE} avec DRVAL1~=~\textit{0} et DRVAL2~=~renseigné avec une valeur correspondant à la profondeur de dragage la plus profonde indiquée dans la zone sur la carte à plus grande échelle. NINFOM et INFORM doivent être renseignés ainsi~: INFORM = \textit{Consulter la carte à plus grande échelle pour connaître la profondeur de dragage dans cette zone.}~; NINFOM = \textit{See the larger scale chart for the depth of dredging in this area.}

	Des informations bathymétriques autres que le plafond de dragage peuvent être représentées à l'intérieur d'une zone draguée. La zone draguée toute entière doit être codée par un \textbf{DRGARE}. Tous les objets relatifs aux autres informations bathymétriques doivent être codés ; les isobathes doivent être codées par des objets de type linéaire \textbf{DEPCNT} "flottants" (ne limitant pas d'objet du Groupe 1) ; les sondes et les dangers cotés doivent être codés avec la valeur de l'attribut EXPSOU appropriée.

	Des informations sur les dragages, connues à un moment donné et non entretenues, sont parfois figurées par des légendes (ex : \textit{12,2 m}) sans aucune référence géométrique. Il est implicitement considéré que l'usager saura associer la profondeur à une zone géographique approximative sans plus de renseignements. Suivant le cas, plusieurs méthodes de codage peuvent être envisagées :

	\begin{itemize}
	\item  Si la légende est isolée, un nota mentionnant la profondeur et le niveau papier équivalente auquel elle est rapportée doit être créé, il est codé dans INFORM et NINFOM d'un objet existant (ex : \textbf{PONTON}, \textbf{SLCONS}, \textbf{HRBFAC}, etc.).

	Exemple de nota :

	INFORM = \textit{This area has a depth of 12,2 metres. The limits of the area and the depth are given for information only. The depth refers to a maintained depth defined for mean working conditions (see French sailing directions).}

	NINFOM = \textit{La profondeur dans cette zone est de 12,2 mètres. Les limites de la zone et la profondeur sont données de façon indicative seulement. La profondeur correspond à une cote théorique définie dans des conditions moyennes d'exploitation (voir Instructions Nautiques).}

	\item  Si plusieurs de ces informations sont réparties dans les bassins ou le long des quais et des appontements, elles doivent alors être assimilées à des sondes et codées dans un même objet \textbf{SOUNDG} avec QUASOU = \textit{11} (non entretenu) et QUAPOS = \textit{4} (position approximative).
	S'il est possible de définir une zone ne contenant que ce type de profondeurs, un objet \textbf{CTNARE} surfacique doit être créé avec INFORM = \textit{Depths not regularly maintained} et NINFOM = \textit{Profondeurs non entretenues.} Si cela n'est pas possible, parce que ces profondeurs sont mélées aux sondes classiques, le nota ci-dessus doit être codé sur l'objet \textbf{SOUNDG}.
\end{itemize}

\end{shom_p}
\section{Zones vérifiées (cf.~S-4 -- B-415)}
\label{SWPARE}
\index{SWPARE}

Si une zone vérifiée doit être codée, l'objet \textbf{SWPARE} doit être utilisé.


\begin{objet}
	Objet géographique~:& Swept area (\textbf{SWPARE}) ~ (A)\\
Attributs~:& \underline{DRVAL1} - plafond contrôlé\\
&QUASOU ~ SOUACC\shom{\footnotemark} \\
&TECSOU -
	\begin{tabular}{|p{0.75\linewidth}}
	\textit{6} - vérifiée par dragage hydrographique\\
	\textit{8} - explorée par système acoustique vertical\\
	\textit{13} - explorée par sondeur latéral\\
	Si renseignée, la valeur de TECSOU doit être l'une des trois ci-dessus.\\
	\end{tabular}\\
&\sout{VERDAT} ~ INFORM ~ NINFOM\\
&SORDAT - \textit{date du levé de contrôle}\\
\end{objet}


Les sondes ponctuelles et isobathes figurées dans ces zones doivent être codées par des objets \textbf{SOUNDG} et \textbf{DEPTCN}. Les attributs QUASOU, SOUACC et TECSOU d'un \textbf{SWPARE} ne s'appliquent qu'à la zone couverte par ce \textbf{SWPARE}. Si la qualité des sondes ponctuelles et des isobathes doit être codée, le méta-objet \textbf{M\_QUAL} doit être utilisé (cf.~§ 2.2.3.1).

Même s'il n'y a pas de sondes ponctuelles ni d'isobathes dans la zone, le \textbf{SWPARE} doit être couvert par un \textbf{DEPARE} ou \textbf{DRGARE}. Si l'information bathymétrique est insuffisante pour coder les attributs DRVAL1 et DRVAL2 du \textbf{DEPARE} ou du \textbf{DRGARE}, DRVAL1 devrait \shom{[doit]} être renseigné par la valeur du plafond de dragage et DRVAL2 devrait \shom{[doit]} être renseigné par une valeur vide (nulle).

Remarques~:

Lorsque la zone vérifiée occupe entièrement la zone couverte par un \textbf{M\_QUAL} et qu'un \textbf{SWPARE} n'est pas défini séparément, DRVAL1 doit être utilisé pour coder le plafond contrôlé. SOUACC peut [ne doit pas] être utilisé sur le \textbf{M\_QUAL} pour spécifier la précision de la valeur du plafond contrôlé codé dans DRVAL1. POSACC ne doit pas être utilisé. Aucune information sur la précision des profondeurs ou des positions ne doit être donnée pour les autres informations bathymétriques pouvant être figurées à l'intérieur de la zone vérifiée.

Lorsque la zone vérifiée occupe entièrement la zone couverte par un \textbf{M\_QUAL} et qu'un \textbf{SWPARE} est défini séparément, DRVAL1 du \textbf{M\_QUAL} doit avoir la même valeur que celle de DRVAL1 du \textbf{SWPARE}. SOUACC peut [ne doit pas] être utilisé sur le \textbf{M\_QUAL} pour spécifier la précision de la valeur du plafond contrôlé. POSACC ne doit pas être utilisé. Aucune information sur la précision des profondeurs ou des positions ne doit être donnée pour les autres informations bathymétriques pouvant être figurées à l'intérieur de la zone vérifiée.

Lorsque la zone couverte par le \textbf{SWPARE} est incluse dans celle couverte par un \textbf{M\_QUAL}, SOUACC ne peut\shom{\footnotemark[3]} être utilisé sur le \textbf{M\_QUAL} que si la précision sur les profondeurs est la même pour le plafond contrôlé et pour les sondes situées à l'extérieur du \textbf{SWPARE}. POSACC \shom{(du \textbf{M\_QUAL})} ne doit être utilisé que pour coder la précision des sondes situées à l'extérieur des limites du \textbf{SWPARE}. Aucune information sur la précision des profondeurs ou des positions ne doit être donnée pour les autres informations bathymétriques pouvant être figurées à l'intérieur de la zone vérifiée.

Les objets \textbf{SWPARE} ne devraient \shom{[doivent]} pas se chevaucher.

\footnotetext{\shom{SOUACC ne doit pas être utilisé.}}

\section{Zones de fonds instables (cf.~S-4 -- B-416)}

Si une zone de fonds instables doit être codée, l'objet \textbf{CTNARE} doit être utilisé (cf.~§ 6.6). Les notes d'avertissement doivent être codées par INFORM \shom{(et NINFOM)} ou TXTDSC.

Une telle zone doit toujours être couverte par un objet \textbf{DEPARE}.

Une zone avec l'indication "Fonds moindres" devrait \shom{[doit]} être codée \shom{selon la méthode suivante} :

\begin{shom_p}
Lorsqu'une légende "Fonds moindres" ne peut être associée à une géométrie surfacique, l'information doit être codée par un \textbf{CTNARE} de type ponctuel, avec les attributs suivants renseignés :\newline
INFORM - \textit{Less water than charted has been reported in the vicinity of this position.}\newline
NINFOM - \textit{Des fonds moindres que ceux indiqués ont été signalés dans le voisinage de cette position.}\newline
Cependant, si cette légende est disposée de façon à matérialiser une zone étendue et s'il est possible de déterminer les limites approximatives de la zone concernée, un \textbf{CTNARE} de type surfacique doit être créé (les limites du \textbf{CTNARE} doivent être masquées). S'il n'est pas possible de limiter la zone, plusieurs objets de type ponctuel répartis sur la légende doivent être créés.\newline
Dans chacun des cas, l'objet spatial associé doit être codé avec QUAPOS = \textit{4} (approximatif).
\end{shom_p}

Si des fonds mobiles (ridens) doivent être codés, l'objet \textbf{SNDWAV} doit être utilisé (cf.~§ 7.2.1).

\section{Zones non hydrographiées ou incomplètement hydrographiées (cf.~S-4 -- B-417 et B-418)}

\subsection{Zones non hydrographiées}
\label{UNSARE}
\index{UNSARE}
Les zones sans aucune information bathymétrique et situées dans une zone couverte par un méta-objet \textbf{M\_COVR} avec l'attribut CATCOV = \textit{1} (couverture cartographique disponible), doivent être codées par des objets \textbf{UNSARE} (voir § 5.8.1.1 ci-dessous pour les exceptions)\textbf{.}


\begin{objet}
Objet géographique~:& Unsurveyed area (\textbf{UNSARE}) ~ (A)\\
Attributs~:& INFORM ~ NINFOM\\
\end{objet}

Remarque~:

Les \textbf{UNSARE} de type surfacique font partie du Groupe 1.

\begin{shom_p}
Lorsqu'une zone non hydrographiée contient au mois une information bathymétrique (\textbf{DEPCNT}, \textbf{OBSTRN}, \textbf{SOUNDG}, \textbf{UWTROC} ou \textbf{WRECKS}), un objet \textbf{M\_QUAL} doit être codé (dans ce cas, la valeur de CATZOC devrait être \textit{5} ou \textit{6}).\newline
Si la zone ne comporte aucune information bathymétrique, aucun objet \textbf{M\_QUAL} ne sera codé.
\end{shom_p}

\subsubsection{Images satellite en tant qu'information source (cf.~S-4 -- B424.7)}

Dans certaines zones, l'information source peut être limitée à de la bathymétrie dans les eaux peu profondes provenant d'images satellite.
Lorsque des gammes de profondeurs peuvent être interpolées à partir des images satellites (par exemple la limite de l'estran, les isobathes 5 mètres ou 10 mètres), et que peu ou aucune information source fiable issue d'un levé hydrographique n'existe dans la zone, la représentation de ces informations dans l'ENC devrait être envisagée.

Si des zones peu profondes dérivées d'images satellites doivent être codées, des objets \textbf{DEPARE} et \textbf{DEPCNT} d'une gamme de profondeurs appropriée devraient \shom{[doivent]} être utilisées. Ceci ne devrait \shom{[doit]} être fait que dans les zones n'ayant pas fait l'objet d'un levé régulier. Les zones couvertes par des informations bathymétriques dérivées d'images satellites devraient \shom{[doivent]} être couvertes par des méta-objets \textbf{M\_QUAL} (voir § 2.2.3.1) avec l'attribut CATZOC renseigné avec la valeur appropriée (c'est-à-dire \textit{4} (zone de confiance C) ou \textit{5} (zone de confiance D) \footnoteshom{Le CATZOC sur les zones couvertes par des informations bathymétriques dérivées d'images satellites doit avoir la valeur 5 (zone de confiance D).}.), et l'attribut TECSOU renseigné avec la valeur \textit{11} (image satellite).



\subsection{Zones incomplètement hydrographiées}

Une zone incomplètement hydrographiée devrait \shom{[doit]} être codée, soit par un \textbf{UNSARE} dans lequel des sondes ou des isobathes (mais pas de zones de profondeurs) peuvent être figurées, ou soit par des \textbf{DEPARE}\footnoteshom{Sauf dérogation mentionnée dans le cahier de définition, l'utilisation de \textbf{DEPARE} pour le codage doit être retenue.}. Les attributs DRVAL1 et DRVAL2 de tels \textbf{DEPARE} devraient \shom{[doivent]} être renseignés par des valeurs explicites.

La zone doit être couverte par des méta-objets \textbf{M\_QUAL} (cf.~§ 2.2.3.1) dont l'attribut CATZOC est renseigné avec la valeur qui convient. Si besoin, des informations supplémentaires peuvent être données par des méta-objets \textbf{M\_SREL}.

Une note d'avertissement devrait \shom{[doit]} aussi être codée par un objet \textbf{CTNARE} de type surfacique (cf.~§ 6.6).

\begin{shom_p}
	Remarques~:

	\begin{itemize}

	\item Sur la carte papier équivalente, on trouve parfois des zones incomplètement hydrographiées dont les limites coupent des isobathes et dans lesquelles des teintes représentent approximativement les zones de profondeurs, sans s'appuyer sur des isobathes.

	Il faut alors traiter séparément la zone incomplètement hydrographiée de la zone hydrographiée adjacente. Les \textbf{DEPARE} adjacentes et identiques seront ensuite fusionnées naturellement si nécessaire.

	La zone incomplètement hydrographiée doit être codée en suivant la règle générale donnée ci-dessus. Pour coder les \textbf{DEPARE} à l'intérieur de cette zone, il faut aller dans le sens de la sécurité et ne pas tenir compte des teintes ne s'appuyant pas sur des isobathes. Par définition, ces teintes sont très approximatives car déduites de très peu de données bathymétriques. En général, la \textbf{DEPARE} la moins profonde (DRVAL1 = \textit{0}, DRVAL2 = \textit{x}) doit être prolongée jusqu'à la limite vers le large de la zone incomplètement hydrogaphiée.

	\item Les informations de la carte papier équivalente relatives aux zones incomplètement hydrographiées et aux zones parsemées de hauts-fonds dangereux doivent être codées de la manière suivante~:
	\end{itemize}

	\begin{enumerate}
	\item \textbf{Codage des zones incomplètement hydrographiées.}\newline
	Les zones qualifiées de «~zones incomplètement hydrographiées~» papier équivalente seront codées par un objet \textbf{CTNARE} avec les attributs INFORM~= This area has been inadequately surveyed. Uncharted shoals may exist. NINFOM~= Cette zone est incomplètement hydrographiée. Il peut y exister des hauts-fonds non représentés sur la carte.

	Dans ces zones, la notion de danger pour la navigation est suffisamment implicite et ne doit pas être précisée dans le nota. Le nota ne fait qu'expliciter le terme de zone incomplètement hydrographiée.

	\item \textbf{Codage des zones parsemées de hauts-fonds dangereux.}\newline
	Les zones qualifiées de «~zones parsemées de hauts-fonds dangereux~» ou «~de hauts-fonds coralliens~» ou «~de nombreuses têtes de corail~» seront codées par un objet \textbf{CTNARE} avec les attributs~INFORM~= Dangerous shoals lie within this area. NINFOM~= Cette zone est parsemée de hauts-fonds dangereux. La nature du fond (corail, roche) n'est pas à renseigner sur le nota INFORM/NINFOM, même si elle est indiquée sur la carte papier équivalente.

	\item \textbf{Existence possible de hauts-fonds coralliens.}\newline
	Le nota à utiliser à l'intérieur du lagon, en dehors des voies recommandées, est~INFORM~= Mariners are warned that uncharted coral shoals may exist outside the areas investigated by side scan sonar (see objects ``Quality of data''). NINFOM~: L'attention des navigateurs est appelée sur l'existence possible de hauts-fonds coralliens ne figurant pas sur la carte en dehors des zones explorées au sondeur latéral (voir les objets «~Quality of data » indiquant la qualité des données bathymétriques).

	\end{enumerate}
\end{shom_p}



\subsection{Bathymétrie dans les zones de représentation minimale sur les cartes papier équivalente}

Dans une ENC, pour un type de navigation donné, lorsqu'il existe des zones dans lesquelles toutes les informations sur les profondeurs ou une grande partie de celles-ci sont omises, elles devraient \shom{[doivent]} être codées suivant l'une des options ci-dessous :

\subsubsection{Zones de non représentation de la bathymétrie}

Il est recommandé aux encodeurs, lorsqu'ils codent des zones bathymétriques à partir de cartes papier contenant des informations minimales sur les profondeurs aux échelles correspondant au type de navigation de l'ENC, de consulter les cartes papier à plus grandes échelles et de généraliser la bathymétrie à partir de ces données. Ceci afin de garantir une information suffisante qui ne soit pas en contradiction avec celle de la couverture à plus grande échelle. Les directives suivantes constituent les recommandations minimales de codage dans de tels cas~:

Lorsqu'une couverture cartographique ENC à plus grande échelle est disponible, les cellules de plus grande échelle de compilation devraient \shom{[doivent]} être examinées pour déterminer l'objet \textbf{DEPARE} le moins profond, autre que la zone d'estran, sur l'ensemble de la zone. Les zones d'estran devraient [devront] ensuite être généralisées à partir de la couverture à plus grande échelle, puis un seul \textbf{DEPARE} peut ensuite être créé sur la zone restante, avec les attributs DRVAL1 et DRVAL2 renseignés par les valeurs obtenues d'après les cartes à plus grande échelle\footnoteshom{DRVAL1 doit prendre la valeur de DRVAL1 du \textbf{DEPARE} le moins profond situé à l'intérieur de la zone et DRVAL2 doit prendre la valeur de DRVAL2 du \textbf{DEPARE} le plus profond. Les valeurs doivent être reportées sur le support d'aide à la codification.}.

Lorsqu'aucune couverture cartographique à plus grande échelle n'est disponible, un seul \textbf{DEPARE} doit être créé pour couvrir la zone.
DRVAL1 de cet objet devrait \shom{[doit]} être renseigné par la plus petite valeur correspondant à la teinte utilisée dans la zone (ex : si la teinte bleue est utilisée pour les zones de profondeurs comprises entre 5 et 20 mètres, DRVAL1 devrait \shom{[doit]} alors prendre la valeur 5).
DRVAL2 devrait \shom{[doit]} prendre la valeur de la plus petite des valeurs des objets adjacents de type surfacique du Groupe 1.

Dans les deux cas, la zone devrait \shom{[doit]} être couverte par un objet\textbf{CTNARE}, dont les limites suivent exactement celles des objetsdu Groupe qui l'entourent (cf.~§ 2.8.2) \shom{(le \textbf{CTNARE} partage donc la géométrie du \textbf{DEPARE} de type surfacique codant la zone)}.

Les encodeurs doivent tenir compte des effets des zones de non-représentation bathymétrique ayant fait l'objet d'une généralisation extrême sur l'affichage de l'ECDIS, lorsque le navigateur fait un zoom arrière à travers les gammes d'échelles.



\subsubsection{Zones de représentation très simplifiée de la bathymétrie}

Dans ces zones, les informations relatives à la bathymétrie (ex : isobathes, dangers, zones rocheuses, roches isolées, natures du fond, zones draguées, zones non hydrographiées) devraient \shom{[doivent]} être codées individuellement.

Un objet \textbf{CTNARE} couvrant l'objet (ou les objets) \textbf{DEPARE} de type surfacique devrait \shom{[doit]} être créé afin de coder une note d'avertissement (cf.~§ 2.8.2).

\subsubsection{\shom{Zones portuaires de représentation très simplifiée de la bathymétrie}}

\begin{shom_p}
	Si, à l'intérieur d'un port, la partie maritime figurée sur la carte papier équivalente est en représentation très simplifiée (en rupture avec le reste de la carte) et qu'il existe ou est programmée une ENC à plus grande échelle de ce port, alors un objet \textbf{DOCARE} (cf.~§ 4.6.6.3) couvert par un \textbf{UNSARE} doit être créé pour coder cette zone ; ni le trait de côte, ni les informations bathymétriques figurées sur la carte papier équivalente n'y doivent être codés. Il est inutile de créer un \textbf{CTNARE}.

	Le choix de coder en \textbf{DOCARE} une zone portuaire de représentation très simplifiée doit être mentionné dans le cahier de définition et les prescriptions sur le codage doivent être reportées sur le support d'aide à la codification.
\end{shom_p}

\subsubsection{\shom{Discontinuité de profondeurs entre les levés hydrographiques (cf.~S-4 -- B416.1)}}
\begin{shom_p}
	Les discontinuités de profondeur entre les levés bathymétriques attenant ou se chevauchant peuvent être causées par~:

	\begin{itemize}
	\item des levés bathymétriques menés dans des zones en constante évolution (voir § 5.7) avec un intervalle de temps important entre les levés, ou
	\item des zones attenantes ayant des différences significatives dans la qualité des données bathymétriques (voir § 2.2.3.1).
	\end{itemize}

	Il n'est pas toujours possible de résoudre, dans le respect des règles de sécurité, les discontinuités significatives de profondeurs par une interpolation approximative des isobathes. Une telle interpolation pourrait compromettre la possibilité pour le cartographe de coder correctement le Groupe 1, sans chevauchement, et sur toute la zone de l'ENC contenant des données.

	S'il est nécessaire d'indiquer ces discontinuités importantes de profondeur, un objet \textbf{UNSARE} «~très étroit~» devrait être codé (voir § 5.8.1).

	La «~surface très étroite~» devrait \shom{[doit]} être large d'environ 0,2 mm à l'échelle de compilation de l'ENC (voir § 2.2.6).

	Remarques~:

	Une indication sur l'objet \textbf{UNSARE} peut être donnée par les attributs INFORM et NINFOM, par exemple INFORM =~\textit{Discontinuity between surveys} et NINFORM \textit{=}~\textit{Discontinuité entre les levés}.

	Afin de fournir une indication au navigateur sur la bathymétrie renseignée la plus fiable dans une zone de profondeur évoluant constamment, le CATZOC du \textbf{M\_QUAL} devra être déclassé à la qualité de la donnée la moins fiable (ou la moins récente).

\end{shom_p}
\end{document}
