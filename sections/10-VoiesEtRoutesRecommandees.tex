\documentclass[../Main.tex]{subfiles}
\begin{document}
\section{Alignements, relèvements et voies recommandées (cf.~S-4 -- B-433 et B-434)}

Si des alignements et des relèvements, qu'ils soient de route, de garde ou lignes de position, doivent être codés, des objets \textbf{NAVLNE} et \textbf{RECTRC} doivent être utilisés ainsi que des objets de type ponctuel concernés (aides à la navigation). Ces règles de codage s'appliquent qu'il s'agisse d'aides visuelles ou d'aides radioélectriques.

Des relations devraient \shom{[doivent]} être définies entre ces objets (cf.~§ 10.1.2 et 15).

NB : En Amérique du Nord, le mot "range" est utilisé à la place des expressions anglaises "transit line" et "leading line".

\subsection{Ligne de route, de garde, de position et voies recommandées}
\label{NAVLNE}
\index{NAVLNE}
Si une ligne de route, de garde ou de position doit être codée, l'objet \textbf{NAVLNE} doit être utilisé.

\begin{objet}
Objet géographique~:& Navigation line (\textbf{NAVLNE}) ~ (L)\\
Attributs~:& \underline{CATNAV} ~ DATEND ~ DATSTA\\
&\underline{ORIENT} - valeur du relèvement, pris du large\\
&PEREND ~ PERSTA ~ STATUS\\
&INFORM - légende telle qu'elle figure sur le document source \textsuperscript{\shom{(1)}}\\
&NINFOM - légende, dans la langue nationale\\
\end{objet}

\label{RECTRC}
\index{RECTRC}
Si une voie recommandée doit être codée, l'objet \textbf{RECTRC} doit être utilisé.

\begin{objet}
Objet géographique~:& Recommended track (\textbf{RECTRC}) ~ (L, A)\\
Attributs~:& \underline{CATTRK} ~ DATEND ~ DATSTA\\
&DRVAL1 - profondeur minimale\textsuperscript{(2)} sur la voie recommandée\\
&\sout{DRVAL2} ~ NOBJNM ~ OBJNAM ~ \underline{ORIENT} ~ PEREND ~ PERSTA ~ QUASOU ~ SOUACC ~ STATUS ~ TECSOU ~ \underline{TRAFIC} ~ \sout{VERDAT}\\
&INFORM - tirant d'eau maximal autorisé\textsuperscript{\shom{(2)}} (ex : \textit{Maximum authorised draft = 14 metres})\\
&NINFOM - \shom{tirant d'eau maximal autorisé, dans la langue nationale (ex : \textit{Tirant d'eau maximal autorisé} \textit{= 14 mètres})}\\
\end{objet}

Remarques~:

La valeur \textit{3} (recommandé) ne devrait pas \shom{[ne doit pas]} être utilisée pour STATUS car, par définition, un \textbf{RECTRC} code une voie recommandée.

La définition S-57 pour la valeur d'attribut CATTRK = \textit{1} (basé sur un système de marques fixes) suggère la possibilité qu'une route soit basée sur une seule marque et un relèvement, ce qui est fréquemment le cas. Par conséquent, l'objet \textbf{RECTRC} devrait \shom{[doit]} égalemement être utilisé pour coder une route droite comprenant une unique structure ou un unique élément naturel pouvant porter des feux et/ou des voyants, et un relèvement spécifié que les navires peuvent suivre en toute sécurité.

Dans le cas d'une voie recommandée à double sens, une seule valeur de direction est codée (dans l'attribut ORIENT)~; l'autre valeur peut en être déduite (c'est la valeur renseignée dans ORIENT +/- 180). La valeur codée dans ORIENT devrait \shom{[doit]} être celle du relèvement pris du large. S'il n'est pas possible de définir une valeur de relèvement pris du large, la valeur inférieure à 180° devrait \shom{[doit]} être utilisée\textsuperscript{\shom{(3)}}.

Lorsque le trafic le long d'une voie recommandée de type linéaire est à sens unique (attribut TRAFIC~= \textit{1,2 ou} \textit{3}), afin d'assurer sur l'ECDIS une représentation correcte de la direction à suivre, la direction résultante de la ligne (tenant compte du sens de numérisation de l'arc et de son sens de lecture dans l'ENC (sous-champ ORNT du champ FSPT = «~direct~» ou «~inverse~») associée au \textbf{RECTRC} doit être la même que celle de la direction du trafic.

\begin{shom_p}
\begin{enumerate}[label=(\arabic*)]
\item Exemples~: NINFOM = \textit{Le château d'eau et la tourelle alignés à 052°} sera traduit par INFORM = \textit{The water tower and the beacon tower in line 052°}. NINFOM = \textit{La flèche de l'église à 155°} sera traduit par INFORM = \textit{Church spire 155°}.
\item Voir S4 - § 434.3 pour les explications sur la différence entre "profondeur minimale" et "tirant d'eau maximal autorisé". 
\item Lorsqu'il existe une direction générale du balisage identifiée (en particulier dans les passes intérieures d'un lagon), le relèvement doit être celui pris dans le sens entrant.
\end{enumerate}
\end{shom_p}


L'utilisation de \textbf{NAVLNE} et \textbf{RECTRC} est définie dans le tableau ci-dessous ainsi que sur la Figure 10 :

\begin{table}[H]
	\centering
	\begin{tabular}{|c|L{0.2\linewidth}|c|c|c|}
		\hline
		\rowcolor[HTML]{C0C0C0} 
		\textbf{Figure 10} & \multicolumn{1}{c|}{\cellcolor[HTML]{C0C0C0}\textbf{}} & \textbf{NAVLNE} & \textbf{RECTRC} & \textbf{Aides à la navigation} \\ \hline
		1 & voie recommandée sur un alignement & CATNAV = 3 & CATTRK = 1 & au moins 2 \\ \hline
		2 & alignement de garde & CATNAV = 1 & aucun & au moins 2 \\ \hline
		3 & ligne de position sur un alignement & CATNAV = 2 & aucun & au moins 2 \\ \hline
		4 & voie recommandée sur un relèvement & CATNAV = 3 & CATTRK = 1 & 1 \\ \hline
		5 & ligne de garde sur un relèvement & CATNAV = 1 & aucun & 1 \\ \hline
		6 & ligne de position sur un relèvement & CATNAV = 2 & aucun & 1 \\ \hline
		7 & voie recommandée ne s'appuyant pas sur des marques fixes & Aucun & CATTRK = 2 & aucun \\ \hline
	\end{tabular}
	\caption{NAVLNE et RECTRC}
	\label{tab:10.1}
\end{table}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{sections/media/image8}
	\caption[Alignements, relèvements et voies recommandées]{Alignements, relèvements et voies recommandées}
	\label{fig:image8}
\end{figure}

Remarques~:

Même si, sur le document source \shom{(carte papier équivalente)}, plusieurs aides à la navigation sont confondues en un seul symbole, un objet géographique particulier doit être créé pour chacune d'elles.

\begin{shom_p}
La géométrie d'un \textbf{NAVLNE} de type linéaire décrivant un alignement ou un relèvement s'appuyant sur des marques fixes répertoriées dans la BDGS doit être construite, si possible, à partir des positions de ces marques prises dans la BDGS. Sinon, cette géométrie sera créée à partir des données provenant de la carte papier équivalente.

En règle générale les voies recommandées sont de géométrie linéaire. Pour les voies recommandées de géométrie surfacique (par exemple celles s'appuyant sur des limites de levés en Nouvelle-Calédonie), l'objet \textbf{FAIRWY} doit être utilisé (cf.~§ 10.4).

	\begin{itemize}
	\item Une mention à l'existence possible de hauts-fonds non cartographiés en dehors des voies recommandées doit être codé par un objet \textbf{CTNARE} de type surfacique à l'extérieur des voies recommandées lorsque celles-ci sont surfaciques, et limité à l'intérieur du lagon quand il en existe un sur la carte. Voir § 5.8.2, point 3 de la remarque 2.
	\item Lorsqu'un alignement marque une zone à laquelle se rapporte un toponyme (une passe par exemple), le toponyme doit être codé par un objet SEAARE et ne doit être codé ni sur le RECTRC, ni sur le NAVLNE.
	\item Toutes les voies recommandées (\textbf{RECTRC}) sont gérées en BDGS. Les \textbf{RECTRC} successives (par ex, segments de voies recommandées dans un chenal) ne sont pas associées dans un objet collection \textbf{C\_AGGR}.
	\end{itemize}
\end{shom_p}

\subsection{Systèmes d'alignement - Relations}

Pour coder un système d'alignement, les objets \textbf{NAVLNE}, \textbf{RECTRC} et les objets décrivant les aides à la navigation devraient\footnoteshom{Un objet \textbf{C\_AGGR} doit toujours être créé pour relier les éléments constitutifs d'un système d'alignement. Pour chacune des aides à la navigation concernées par l'alignement, seul l'objet maître codant le support doit être pris en compte par le \textbf{C\_AGGR}. Si le support n'est pas codé, l'objet \textbf{LIGHTS} décrivant le secteur du feu visible sur l'alignement ou celui \textbf{RTPBCN} décrivant la balise répondeuse radar doit alors être pris en compte par le \textbf{C\_AGGR}.} être reliés par un objet collection \textbf{C\_AGGR} (cf.~§ 15).

Cet objet "agrégation" peut\footnoteshom{Sauf dérogation mentionnée dans le cahier de définition, l'objet \textbf{C\_ASSO} ne doit pas être utilisé.} aussi être associé, par un objet \textbf{C\_ASSO} (cf.~§ 15), aux dangers (ex : \textbf{OBSTRN}, \textbf{WRECKS}, \textbf{UWTROC}) marqués par la ligne de garde ou la ligne de position (voir Figure 11).

\begin{figure}[H]
	\centering
	\includegraphics{sections/media/image9}
	\caption[Système d'alignement]{Système d'alignement}
	\label{fig:10.2}
\end{figure}


\subsection{Distances mesurées \texorpdfstring{\shom{(base de vitesse)}}{(base de vitesse)} (cf.~S-4 -- B-458)}

Si la route à suivre est sur un alignement ou sur un relèvement, elle doit être codée d'après les règles décrites dans le tableau 10.1 et la Figure 10 ci-dessus (cas 1 ou 4). Si la route n'est pas sur un alignement ou un relèvement, elle doit être codée par seulement un \textbf{NAVLNE} avec CATNAV~=~non renseigné (valeur nulle). Dans tous les cas, si la distance mesurée doit être codée, les attributs INFORM (ex \textit{: Measured distance = 1450 metres)} \shom{et NINFOM (ex \textit{: Distance mesurée = 1450 mètres})} doivent être utilisés.

Si les lignes de position marquant les extrémités de la distance mesurée doivent être codées, des objets \textbf{NAVLNE} doivent être utilisés avec CATNAV = \textit{2} (ligne de position).

Si les balises doivent être codées, des objets \textbf{BCNSPP} doivent être utilisés avec CATSPM = \textit{17} (marque de distance mesurée).

Lorsqu'un système complet marquant une distance mesurée existe à l'intérieur d'une cellule, chacune des lignes de position doit être liée à ses balises par un objet collection \textbf{C\_AGGR} (cf.~§ 15). Ces deux objets "agrégation" et la route à suivre doivent être liés par un autre objet collection \textbf{C\_AGGR}.

\section{Organisation du trafic}

\subsection{Dispositifs de séparation du trafic.}

Si un dispositif de séparation du trafic doit être codé, des objets \textbf{DWRTCL, DWRTPT, ISTZNE, PRCARE, TSELNE}, \textbf{TSEZNE}, \textbf{TSSBND}, \textbf{TSSCRS}, \textbf{TSSLPT}, \textbf{TSSRON} doivent être utilisés ainsi que des objets décrivant les aides à la navigation.

Le codage des relations entre ces objets est défini au § 10.2.3.

Pour les conseils de codage sur les annonces anticipées des changements relatifs aux dispositifs de séparation du trafic, voir § 2.6.1.1.

\subsubsection{Couloirs de circulation (cf.~S-4 -- B-435.1)}
\label{TSSLPT}
\index{TSSLPT}
Dans un dispositif de séparation du trafic, un couloir de circulation complet est constitué d'une ou de plusieurs sections à l'intérieur desquelles le trafic se fait suivant une direction établie. Si ces sections doivent être codées, l'objet \textbf{TSSLPT} doit être utilisé.

\begin{objet}
Objet géographique~:& Traffic separation scheme lane part (\textbf{TSSLPT}) ~ (A)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA\\
&\underline{ORIENT} - direction établie du trafic\\
&RESTRN ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

Remarque~:

Dans les jonctions autres que les croisements et les ronds-points, un \textbf{TSSLPT} distinct doit être codé. Sur cet objet, l'attribut ORIENT ne doit pas être codé pour éviter de laisser croire qu'un couloir a la priorité sur l'autre (cf.~INT1 - M22). Une note d'avertissement peut\footnoteshom{L'information est codée si un avertissement est figuré sur la carte papier équivalente.} être codée au moyen des attributs INFORM \shom{(et NINFOM)} ou TXTDS\footnoteshom{TXTDSC ne doit pas être utilisé.}. Dans certains cas, une zone de prudence est établie à la jonction ou à l'intersection des routes (cf.~§ 10.2.1.8).

\begin{figure}[H]
	\centering
	\includegraphics{sections/media/image10}
	\caption[Jonction]{Jonction}
	\label{fig:10.3}
\end{figure}

La direction de la portion du couloir de circulation est définie par la ligne centrale du couloir et est associée à la direction générale du trafic dans le couloir du dispositif de séparation du trafic.

\subsubsection{Limites de dispositifs de séparation du trafic (cf.~S-4 -- B-435.1)}
\label{TSSBND}
\index{TSSBND}
L'objet \textbf{TSSBND} ne doit être utilisé que pour coder la limite extérieure d'un couloir de circulation ou d'un rond-point de dispositif de séparation du trafic.

\begin{objet}
Objet géographique~:& Traffic separation scheme boundary (\textbf{TSSBND}) ~ (L)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

Remarque :

L'objet \textbf{TSSBND} de doit pas être utilisé pour coder la limite entre un couloir de dispositif de séparation du trafic ou un rond-point et une zone de séparation du trafic ; ou entre une zone de séparation du trafic et une zone de navigation côtière.


\subsubsection{Lignes de séparation du trafic (cf.~S-4 -- B-435.1)}
\label{TSELNE}
\index{TSELNE}
L'objet \textbf{TSELNE} ne doit être utilisé que pour coder la ligne de séparation entre deux couloirs de circulation ou entre un couloir de circulation et une zone de navigation côtière.

\begin{objet}
Objet géographique~:& Traffic separation line (\textbf{TSELNE}) ~ (L)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\shom{Une ligne de séparation du trafic est représentée sur la carte papier équivalente par une bande tramée de 3~mm de large. Elle ne doit pas être codée par un objet de type surfacique couvrant la bande tramée, mais par un \textbf{TSELNE} de type linéaire dont la géométrie est conforme aux données de la BDGS ou, par défaut, à la géométrie définie dans le texte officiel. Dans ce dernier cas, les prescriptions sur le codage des \textbf{TSELNE} doivent être reportées dans le dossier de définition.}

\subsubsection{Zones de séparation du trafic (cf.~S-4 -- B-435.1)}
\label{TSEZNE}
\index{TSEZNE}
L'objet \textbf{TSEZNE} ne doit être utilisé que pour coder la zone de séparation entre deux couloirs de circulation ou entre un couloir de circulation et une zone de navigation côtière ou encore pour coder la zone centrale d'un rond point.

\begin{objet}
Objet géographique~:& Traffic separation zone (\textbf{TSEZNE}) ~ (A)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\subsubsection{Croisements de dispositifs de séparation du trafic (cf.~S-4 B-435.1)}
\label{TSSCRS}
\index{TSSCRS}
L'objet \textbf{TSSCRS} ne doit être utilisé que pour coder une zone de croisement entre au moins quatre couloirs de circulation.

\begin{objet}
Objet géographique~:& Traffic separation scheme crossing (\textbf{TSSCRS}) ~ (A)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ RESTRN ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

Remarques~:

\begin{itemize}
\item Les jonctions autres que des croisements ou des ronds-points devraient \shom{[doivent]} être codées par des \textbf{TSSLPT} (cf.~§ 10.2.1.1).
\item Un \textbf{TSSCRS} ne doit pas chevaucher un objet \textbf{TSEZNE} situé au centre du croisement.
\item Dans certains cas, une zone de prudence est établie à la jonction ou à l'intersection des routes (cf.~§~10.2.1.8).
\end{itemize}

\subsubsection{Ronds-points de dispositifs de séparation du trafic (cf.~S-4 -- B-435.1)}
\label{TSSRON}
\index{TSSRON}
L'objet \textbf{TSSRON} ne doit être utilisé que pour coder la zone à l'intérieur de laquelle le trafic est orienté dans le sens contraire des aiguilles d'une montre autour d'un point ou d'une zone spécifié.

\begin{objet}
Objet géographique~:& Traffic separation scheme roundabout (\textbf{TSSRON}) ~ (A)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ RESTRN ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

Remarques~:

\begin{itemize}
\item Les jonctions autres que les intersections et ronds-points devraient \shom{[doivent]} être codées par la classe d'objet \textbf{TSSLPT} (cf.~§ 10.2.1.1).
\item Un \textbf{TSSRON} ne doit pas chevaucher un objet \textbf{TSEZNE} situé au centre du rond-point.
\item Dans certains cas, une zone de prudence est établie à la jonction ou à l'intersection des routes (cf.~§~10.2.1.8).
\end{itemize}

\subsubsection{Zones de navigation côtière (cf.~S-4 -- B-435.1)}
\label{ISTZNE}
\index{ISTZNE}
L'objet \textbf{ISTZNE} ne doit être utilisé que pour coder la zone identifiée située entre la limite côté terre d'un dispositif de séparation de trafic et la côte.

\begin{objet}
Objet géographique~:& Inshore traffic zone (\textbf{ISTZNE}) ~ (A)\\
Attributs~:& CATTSS ~ DATEND ~ DATSTA ~ RESTRN ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\subsubsection{Zones de prudence (cf.~S-4 -- B-435.2)}
\label{PRCARE}
\index{PRCARE}
L'objet \textbf{PRCARE} ne doit être utilisé que pour coder une zone dont les limites sont définies, où les navires doivent naviguer avec une attention particulière, et à l'intérieur de laquelle la direction du trafic peut être recommandée.

\begin{objet}
Objet géographique~:& Precautionary area (\textbf{PRCARE}) ~ (P, A)\\
Attributs~:& DATEND ~ DATSTA ~ RESTRN ~ STATUS ~ \underline{INFORM} ~ NINFOM ~ \underline{TXTDSC} ~ NTXDS\\
\end{objet}

Remarque~:

\begin{itemize}
\tightlist
\item Un \textbf{PRCARE} peut chevaucher d'autres éléments du dispositif de séparation du trafic (ex : \textbf{TSSRON}, \textbf{TSSLPT},
\textbf{TSSCRS}).
\end{itemize}

\subsection{Routes en eau profonde (cf.~S-4 -- B-435.3)}

\subsubsection{Sections de routes en eau profonde}
\label{DWRTPT}
\index{DWRTPT}
Une route en eau profonde complète est constituée d'une ou de plusieurs sections~à l'intérieur desquelles le trafic se fait soit suivant une direction établie pour un trafic à sens unique ou soit suivant une direction établie et celle opposée pour un trafic à double sens. Si ces sections doivent être codées, l'objet \textbf{DWRTPT} doit être utilisé.

\begin{objet}
Objet géographique~:& Deep water route part (\textbf{DWRTPT}) ~ (A)\\
Attributs~:& DATEND ~ DATSTA\\
&\underline{DRVAL1} - profondeur minimale\\
&\sout{DRVAL2}\\
&NOBJNM\\
&OBJNAM - ne devrait \shom{[doit]} être utilisé que seulement si l'objet individuel n'est pas lié par un objet collection\\
&\underline{ORIENT} - direction du trafic\\
&QUASOU ~ RESTRN ~ SOUACC ~ STATUS ~ TECSOU ~ \underline{TRAFIC} ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
\end{objet}

Remarques~:
\begin{itemize}
\item La route doit être couverte par des objets \textbf{DEPARE}.
\item Une route en eau profonde peut chevaucher un objet \textbf{TSSLPT}.
\item L'orientation de la route en eau profonde est définie par l'axe de la section concernée et la direction générale du trafic dans la route en eau profonde.
\item Pour coder une route en eau profonde complète, les objets \textbf{DWRTCL}, \textbf{DWRTPT}, ainsi que les objets décrivant les aides à la navigation (s'ils sont mentionnés dans la réglementation définissant la route) peuvent \shom{[doivent]} être liés par un objet \textbf{C\_AGGR} (cf.~§ 15). Si le nom de la route en eau profonde complète doit être indiqué, il devrait \shom{[doit]} être codé par un objet \textbf{SEAARE} (cf.~§ 8), ou en renseignant OBJNAM du plus représentatif des objets de la route en eau profonde. Si une information textuelle sur la route en eau profonde doit être codée, l'objet \textbf{M\_NPUB} (cf.~§ 2.5) devrait \shom{[doit]} être utilisé, avec les attributs INFORM/NINFOM et/ou TXTDSC/NTXTDS (cf.~§ 2.3), ou l'objet \textbf{CTNARE} (cf.~§ 6.6) doit être utilisé si l'information est jugée essentielle pour la sécurité de la navigation.
\item Des routes en eau profonde peuvent être associées à d'autres mesures d'organisation du trafic, telles que des dispositifs de séparation du trafic, pour composer un système complet d'organisation du trafic. Pour coder la relation entre les mesures d'organisation du trafic, les \textbf{C\_AGGR} définissant chaque mesure d'organisation du trafic particulière du système (ou l'objet approprié, si la mesure d'organisation consiste en un seul objet) peuvent \shom{[doivent]} être assemblés dans un \textbf{C\_AGGR} pour former une relation hiérarchique (cf.~§ 15). Les éléments individuels composant chacun une mesure d'organisation du trafic particulière ne doivent pas être assemblés dans un seul \textbf{C\_AGGR}.
\end{itemize}

\subsubsection{Lignes axiales de routes en eau profonde}
\label{DWRTCL}
\index{DWRTCL}
Si la ligne axiale d'une route en eau profonde dont la largeur n'est pas définie explicitement doit être codée, l'objet \textbf{DWRTCL} doit être utilisé.

\begin{objet}
Objet géographique~:& Deep water route centreline (\textbf{DWRTCL}) ~ (L)\\
Attributs~:& \underline{CATTRK} ~ DATEND ~ DATSTA\\
&DRVAL1 - profondeur minimale\\
&\sout{DRVAL2}\\
&NOBJNM\\
&OBJNAM - ne devrait \shom{[doit]} être utilisé que seulement si l'objet individuel n'est pas lié par un objet collection\\
&\underline{ORIENT} ~ QUASOU ~ SOUACC ~ STATUS ~ TECSOU ~ \underline{TRAFIC} ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
\end{objet}

Remarque :

\begin{itemize}
\item Dans le cas d'une ligne axiale de route en eau profonde à double sens, une seule valeur de direction doit être codée (par l'attribut ORIENT)~; l'autre valeur peut en être déduite (ORIENT +/- 180). La valeur de la direction codée dans ORIENT devrait \shom{[doit]} être la valeur du relèvement pris du large vers la terre. S'il n'est pas possible de définir une telle direction, la valeur inférieure à 180 devrait être utilisée.
\item Lorsque le trafic est à sens unique (attribut TRAFIC~= \textit{1,2 ou} \textit{3}), afin d'assurer sur l'ECDIS une représentation correcte de la direction à suivre, la direction résultante de la ligne (tenant compte du sens de numérisation de l'arc et de son sens de lecture \shom{(sous-champ ORNT du champ FSPT = «~direct~» ou «~inverse~»)} associée au \textbf{DWRCTL} doit être la même que celle de la direction du trafic.
\end{itemize}

\subsection{Systèmes d'organisation du trafic}

Pour coder un système complet d'organisation du trafic, les objets \textbf{DWRTCL, DWRTPT, ISTZNE, PRCARE, TSELNE}, \textbf{TSEZNE}, \textbf{TSSBND}, \textbf{TSSCRS}, \textbf{TSSLPT}, \textbf{TSSRON} ainsi que les objets décrivant les aides à la navigation (s'il sont mentionnés dans la réglementation définissant le dispositif de séparation du trafic ou la route en eau profonde), doivent être liés par un objet collection \textbf{C\_AGGR} (cf.~§ 15). Si le nom du DST complet doit être indiqué, il devrait \shom{[doit]} être codé par un objet \textbf{SEAARE} (cf.~§ 8), ou en renseignant OBJNAM du plus représentatif des objets du DST. Si une information textuelle sur le DST doit être codée, l'objet \textbf{M\_NPUB} (cf.~§ 2.5) devrait \shom{[doit]} être utilisé, avec les attributs INFORM/NINFOM et/ou TXTDSC/NTXTDS (cf.~§ 2.3), ou l'objet \textbf{CTNARE} (cf.~§ 6.6) devrait \shom{[doit]} être utilisé, si l'information est jugée essentielle pour la sécurité de la navigation.

Remarque :

\begin{itemize}
\tightlist
\item Un dispositif de séparation du trafic peut être associé à d'autres mesures d'organisation du trafic, telles que des routes en eau profonde, des routes à double sens de circulation ou d'autres dispositifs de séparation du trafic, pour composer un système complet d'organisation du trafic. Pour coder la relation entre les mesures d'organisation du trafic, les \textbf{C\_AGGR} définissant chaque mesure d'organisation du trafic particulière du système (ou l'objet approprié, si la mesure d'organisation consiste en un seul objet) peuvent \shom{[doivent]} être assemblés dans un \textbf{C\_AGGR} pour former une relation hiérarchique (cf.~§ 15). Les éléments individuels composant chacun une mesure d'organisation du trafic différente ne doivent pas être assemblés dans un seul \textbf{C\_AGGR}.
\end{itemize}

\subsection{Routes recommandées (cf.~S-4 -- B-435.4)}
\label{RCRTCL}
\index{RCRTCL}
Si une ligne axiale de voie recommandée doit être codée, l'objet \textbf{RCRTCL} doit être utilisé.

\begin{objet}
Objet géographique~:& Recommended route centreline (\textbf{RCRTCL}) ~ (L)\\
Attributs~:& \underline{CATTRK} ~ DATEND ~ DATSTA\\
&DRVAL1 - profondeur minimale\\
&\sout{DRVAL2} ~ NOBJNM ~ OBJNAM ~ ORIENT ~ PEREND ~ PERSTA ~ QUASOU ~ SOUACC ~ STATUS ~ TECSOU ~ TRAFIC ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
\end{objet}

Remarques :

\begin{itemize}
\item Dans le cas d'une ligne axiale de route en eau profonde à double sens, une seule valeur de direction doit être codée (par l'attribut ORIENT)~; l'autre valeur peut en être déduite (ORIENT +/- 180). La valeur de la direction codée dans ORIENT devrait \shom{[doit]} être la valeur du relèvement pris du large vers la terre. S'il n'est pas possible de définir une telle direction, la valeur inférieure à 180 devrait \shom{[doit]} être utilisée.
\item Lorsque le trafic est à sens unique (attribut TRAFIC~= \textit{1,2 ou} \textit{3}), afin d'assurer sur l'ECDIS une représentation correcte de la direction à suivre, la direction résultante de la ligne (tenant compte du sens de numérisation de l'arc et de son sens de lecture (sous-champ ORNT du champ FSPT = «~direct~» ou «~inverse~») associée au \textbf{RCRTCL} doit être la même que celle de la direction du trafic.
\end{itemize}

\subsection{Directions recommandées du trafic (cf.~S-4 -- B-435.5)}
\label{RCTLPT}
\index{RCTLPT}
La classe d'objets \textbf{RCTLPT} devrait \shom{[doit]} être utilisée pour coder les zones dans lesquelles une direction recommandée du trafic a été définie~:

\begin{itemize}
\item entre deux dispositifs de séparation du trafic (cf.~INT1 - M26.1)
\item à l'entrée d'un dispositif de séparation du trafic
\item à l'extérieur et le long d'une route en eau profonde (cf.~INT1 - M26.2)
\end{itemize}

Objet géographique~: Recommended traffic lane part (\textbf{RCTLPT}) (P, A)

Attributs~: DATEND DATSTA \underline{ORIENT} STATUS INFORM NINFOM

Remarque~:

\begin{itemize}
\item Lorsque la zone n'est pas définie, un objet de type ponctuel devrait \shom{[doit]} être créé.
\item La direction de la partie du couloir de navigation recommandé est définie par la ligne axiale et est associée à la direction générale du trafic dans le couloir de navigation.
\item \shom{Les objets \textbf{RCTLPT} ne doivent pas être intégrés aux \textbf{C\_AGGR} décrivant le dispositif de séparation du trafic et ne doivent pas être intégrés en BDGS.}
\end{itemize}

\subsection{Routes à double sens de circulation (cf.~S-4 -- B-435.6)}
\label{TWRTPT}
\index{TWRTPT}
Une route à double sens est constituée de sections à l'intérieur desquels le trafic se fait suivant deux directions opposées le long d'un relèvement. Si ces sections doivent être codés, l'objet \textbf{TWRTPT} doit être utilisé. Ces sections sont généralement à double sens, mais certaines peuvent être à sens unique (cf.~INT1 - M28.2).

\begin{objet}
Objet géographique~:& Two-way route part (\textbf{TWRTPT}) ~ (A)\\
Attributs~:& CATTRK ~ DATEND ~ DATSTA\\
&DRVAL1 - profondeur minimale\\
&\sout{DRVAL2} ~ \underline{ORIENT} ~ QUASOU ~ SOUACC ~ STATUS ~ TECSOU ~ \underline{TRAFIC} ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\
\end{objet}

\begin{figure}[H]
	\centering
	\includegraphics{sections/media/image11}
	\caption[Tronçon à sens unique dans une route à double sens]{Tronçon à sens unique dans une route à double sens}
	\label{fig:image11}
\end{figure}

Si une route à double sens avec des sections à sens unique doit être codée, des \textbf{TWRTPT} distincts doivent être créés pour coder les sections avec TRAFIC = \textit{3} (sens unique) ou TRAFIC = \textit{4} (double sens). Dans les sections à sens unique, l'attribut ORIENT doit indiquer la direction effective du trafic, pas celle opposée. Dans les sections à double sens, ORIENT peut indiquer l'une ou l'autre des directions.

Remarques~:

\begin{itemize}
\item L'orientation de la route à double sens est définie par l'axe de la section concernée et la direction générale du trafic dans la route à double sens.
\item Pour coder une route à double sens complète, les objets \textbf{TWRTPT} peuvent \shom{[doivent]} être assemblés en utilisant un objet collection \textbf{C\_AGGR} (cf § 15). Si le nom de la route à double sens complète doit être indiqué, il devrait \shom{[doit]} être codé par un objet \textbf{SEAARE} (cf.~§ 8), ou en renseignant OBJNAM du plus représentatif des objets de la route. Si une information textuelle sur la route route à double sens doit être codée, l'objet \textbf{M\_NPUB} (cf.~§ 2.5) devrait \shom{[doit]} être utilisé, avec les attributs INFORM / NINFOM et/ou TXTDSC / NTXTDS (cf.~§ 2.3), ou l'objet \textbf{CTNARE} (cf.~§ 6.6) doit être utilisé, si l'information est jugée essentielle pour la sécurité de la navigation.
\item Des routes à double sens peuvent être associées à d'autres mesures d'organisation du trafic, telles que des dispositifs de séparation du trafic, pour composer un système complet d'organisation du trafic.
Pour coder la relation entre les mesures d'organisation du trafic, les \textbf{C\_AGGR} définissant chaque mesure d'organisation du trafic particulière du système (ou l'objet approprié, si la mesure d'organisation consiste en un seul objet) peuvent \shom{[doivent]} être assemblés dans un \textbf{C\_AGGR} pour former une relation hiérarchique (cf.~§ 15). Les éléments individuels composant chacun une mesure d'organisation du trafic particulière ne doivent pas être assemblés dans un seul \textbf{C\_AGGR}.
\end{itemize}

\subsection{Zones à éviter (cf.~S-4 -- B-435.7)}

Si une zone à éviter doit être codée, l'objet \textbf{RESARE} doit être utilisé (cf.~§ 11.1) avec RESTRN = \textit{14} (zone à éviter)\textit{.} Une zone à éviter située autour d'une aide à la navigation doit en plus être codée avec CATREA = \textit{12} (zone de protection du balisage)\textit{.}

\ohi{(EB 58)~La valeur 14 (zone à éviter) ne peut être utilisée sur l'attribut RESTRN que pour un objet \textbf{RESARE} et ne doit être utilisée sur aucun autre objet acceptant l'attribut RESTRN, ni sur un objet \textbf{RESARE} ne correspondant pas à une zone définie par l'OMI comme «~zone à éviter~» dans le cadre d'une mesure d'organisation du trafic. Les zones dans lesquelles l'entrée est interdite ou règlementée, autres que celles définies par l'OMI, devraient \shom{[doivent]} être codées par l'objet approprié avec l'attribut RESTRN = 7 (entrée interdite) ou 8 (entrée règlementée).}

\section{Navires transbordeurs (cf.~S-4 -- B-438)}
\label{FERYRT}
\index{FERYRT}
Si une route pour navire transbordeur doit être codée, l'objet \textbf{FERYRT} doit être utilisé.

\begin{objet}
Objet géographique~:& Ferry route (\textbf{FERYRT}) ~ (L, A)\\
Attributs~:& \underline{CATFRY} ~ DATEND ~ DATSTA ~ NOBJNM ~ OBJNAM ~ PEREND ~ PERSTA ~ STATUS ~ INFORM ~ NINFOM\\
\end{objet}

\section{Chenaux (cf.~S-4 -- B-432.1 et B-434.5)}
\label{FAIRWY}
\index{FAIRWY}
Si un chenal doit être codé, l'objet \textbf{FAIRWY} doit être utilisé.

\begin{objet}
Objet géographique~:& Fairway (\textbf{FAIRWY} ~ (A)\\
Attributs~:& DATEND ~ DATSTA\\
&DRVAL1 - profondeur minimale dans le chenal\\
&NOBJNM ~ OBJNAM ~ ORIENT ~ QUASOU ~ RESTRN ~ SOUACC ~ STATUS ~ TRAFIC ~ \sout{VERDAT} ~ INFORM ~ NINFOM\\

\end{objet}

\begin{shom_p}
La classe d'objets \textbf{FAIRWY} doit être réservée~aux chenaux faisant l'objet :

\begin{itemize}
\item d'une réglementation (en magenta sur les cartes papier), \item ou d'une délimitation à caractère hydrographique (en noir sur les cartes papier)~; par exemple, les chenaux délimités par les secteurs de feux dans le Raz-de-Sein ou les voies recommandées s'appuyant sur des limites de levés en Nouvelle-Calédonie. Le moyen mis en oeuvre pour le contrôle de la voie recommandée (sondeur latéral par exemple) n'est pas codé en utilisant les attributs INFORM/NINFOM de l'objet \textbf{FAIRWAY} mais en utilisant l'attribut TECSOU de l'objet \textbf{M\_QUAL}. S'il existe un seuil de profondeur minimale ou de recherche associée à la zone contrôlée, celui-ci sera alors explicitement indiqué par les attributs INFORM/NINFOM de l'objet \textbf{FAIRWAY}. Il sera également codé par l'attribut DRVAL1, respectivement DRVAL2 de l'objet \textbf{M\_QUAL} (voir § 2.2.3.1).
\item de zones explorées par sondeur latéral qui ne sont pas identifiées en tant que "voie recommandée" ou zone de mouillage sur la carte papier équivalente mais qui feraient l'objet d'un seuil et/ou s'il n'existe pas d'objet \textbf{CTNARE} de type surfacique délimitant les zones explorées au sondeur latéral. Lorsqu'il n'existe pas de seuil et qu'il existe un objet \textbf{CTNARE} délimitant la zone contrôlée au sondeur latéral, le codage de cette zone (pas identifiée en tant que "voie recommandée" ou zone de mouillage) se fera uniquement avec l'objet \textbf{M\_QUAL} (voir § 2.2.3.5).
\end{itemize}

La classe d'objets \textbf{FAIRWY} est gérée en BDGS.

Les autres chenaux, généralement figurés sur la carte papier équivalente par un toponyme seulement, doivent être codés par des objets \textbf{SEAARE}.

Sur les côtes françaises, des chenaux d'accès et des zones d'attente destinés aux navires transportant des hydrocarbures ou d'autres substances dangereuses ont été définis. Ces chenaux doivent être codées par des \textbf{FAIRWY.} La description est adaptée à la réglementation particulière de chacun d'eux, elle est déduite des données de la BDGS complétées des informations des Instructions Nautiques. Un exemple de description sémantique est donné ci-dessous :

TRAFIC - \textit{4 -} double sens

STATUS \textit{- 9} - caractère obligatoire

RESTRN - \textit{1,8,25} - mouillage interdit, navigation réglementée, stationnement interdit

OBJNAM - \textit{Brest access channel}

NOBJNM - \textit{Chenal d'accès à Brest}

NINFOM - \textit{Chenal d'accès obligatoire pour les navires transportant des hydrocarbures ou d'autres substances dangereuses (voir Instructions Nautiques). Il est interdit de mouiller ou de stationner dans cette zone.}

INFORM - \textit{Compulsory port access channel for vessels laden with hydrocarbons or other dangerous substances (see French sailing directions). Anchoring or stopping is prohibited in this area.}

\end{shom_p}
Remarques~:

\begin{itemize}

\item Un objet collection \textbf{C\_AGGR} ou \textbf{C\_ASSO}\footnoteshom{Sauf dérogation mentionnée dans le cahier de définition, l'objet \textbf{C\_ASSO} ne doit pas être utilisé.} (cf.~§ 15) devrait\footnoteshom{Seuls les différents tronçons ou voies composant un même chenal doivent être liés par un \textbf{C\_AGGR}. Les autres éléments associés, tels que le balisage, les zones draguées ou les zones réglementées ne doivent pas être liés.} être créé pour lier le chenal aux aides à la navigation, aux voies recommandées, aux zones draguées et aux autres zones réglementées.
\item Lorsque les balises ou les bouées marquant un chenal sont décalées par rapport aux limites réelles du chenal, cela devrait \shom{[doit]} être indiqué par l'attribut INFORM (et NINFOM) du \textbf{FAIRWY}.
\item Un chenal balisé est parfois figuré sur la carte papier équivalente par une simple légende "Chenal balisé" ou "Balisé", sans que les marques de balisage soient cartographiées. Dans ce cas, si on ne peut pas l'associer à un objet existant (par exemple : \textbf{FAIRWY}, \textbf{DRGARE}, \textbf{RECTRC},\ldots à l'exception de \textbf{SEAARE}) la légende doit être codée par un objet ponctuel \textbf{CTNARE} avec INFORM = \textit{Buoyed channel} et NINFOM = \textit{Chenal balisé}.
\item Pour des raisons d'affichage sur l'ECDIS, la valeur d'attribut TRAFIC = \textit{4} (double sens) ne doit pas être utilisée lorsque ORIENT n'est pas codé. 
\end{itemize}


\section{Voie de circulation archipélagique}

Si une voie de circulation archipélagique doit être codée, les objets \textbf{ARCSLN} et/ou \textbf{ASLXIS} doivent être utilisés, ainsi qu'éventuellement des objets décrivant les aides à la navigation.

Le caractère particulier des voies de circulation archipélagique est spécifié dans l'Article 53 de la convention des Nations Unies sur le droit de la mer et dans la partie H de l'ouvrage Organisation du trafic maritime de l'OMI (Ship' Routeing) traitant des dispositions générales.

Le codage des relations entre ces objets est défini au § 10.5.3.

Remarque~:

\begin{itemize}
\item Dans certains cas, des informations précises sur les axes (\textbf{ASLXIS}) sont les seules disponibles, et il se peut donc que l'extension de la voie de circulation (\textbf{ARCSLN}) ne puisse pas être codée.
\end{itemize}

\subsection{Voie de circulation archipélagique (cf.~S-4 -- B-435.10)}
\label{ARCSLN}
\index{ARCSLN}
L'objet \textbf{ARCSLN} doit seulement être utilisé pour coder la \underline{zone} couverte par la voie de circulation archipélagique.

\begin{objet}
Objet géographique~:& Archipelagic Sea Lane (\textbf{ARCSLN}) ~ (A)\\
Attributs~:& DATEND ~ DATSTA ~ \underline{NATION} ~ NOBJNM ~ OBJNAM\\
\end{objet}

\subsection{Ligne axiale de voie de circulation archipélagique (cf.~S-4 -- B-435.10)}
\label{ASLXIS}
\index{ASLXIS}
L'objet \textbf{ASLXIS} doit seulement être utilisé pour coder la ligne axiale définissant une voie de circulation archipélagique.


\begin{objet}
Objet géographique~:& Archipelagic Sea Lane axis (\textbf{ASLXIS}) ~ (L)\\
Attributs~:& DATEND ~ DATSTA ~ \underline{NATION} ~ NOBJNM ~ OBJNAM\\
\end{objet}

\subsection{Systèmes de voies de circulation archipélagique.}

Pour coder un système de voies de circulation archipélagique, les objets \textbf{ARCSLN}, \textbf{ASLXIS} ainsi que les objets décrivant les aides à la navigation (s'il sont mentionnés dans la réglementation définissant la voie de circulation archipélagique), doivent être liés par un objet collection \textbf{C\_AGGR} (cf.~§ 15). Si le nom du système de voies de circulation archipélagique doit être indiqué, il devrait \shom{[doit]} être codé par un objet \textbf{SEAARE} (cf.~§ 8), ou en renseignant OBJNAM du plus représentatif des objets du système. Si une information textuelle sur le système de voies de circulation archipélagique doit être codée, l'objet \textbf{M\_NPUB} (cf.~§ 2.5) devrait \shom{[doit]} être utilisé, avec les attributs INFORM/NINFOM et/ou TXTDSC/NTXTDS (cf.~§ 2.3), ou l'objet \textbf{CTNARE} (cf.~§ 6.6) doit être utilisé, si l'information est jugée essentielle pour la sécurité de la navigation.

\end{document}