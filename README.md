# S-57 APPENDICE B.1
## Annexe A - Utilisation du Catalogue d'Objets pour les ENC
### Version latex de l'UOC Shom - V3.0

This project tracks conversion of French version of *IHO S-57 Appendix B.1, Annex A - Use of the Object Catalogue for ENC - UOC V3.0*, from MS Word (docx) to Latex

It is only intended to be used by [Shom](https://www.shom.fr/)

### Tools
This project is realised with the folowing tools/environnment

#### Conversion from docx to markdown and markdown to tex
 - Pandoc : https://pandoc.org
 - Typora : https://typora.io

#### In-depth conversion and adaptation
 -  Miktex distribution : https://miktex.org
 -  TeXstudio : https://texstudio.org
 -  GitHub Desktop : https://desktop.github.com
 -  Tables assistant : https://www.tablesgenerator.com/latex_tables

##### Graphics and flowcharts
 - 	Inkscape : https://inkscape.org

ModDate 09/06/2012

